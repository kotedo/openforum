#!/bin/bash
APPSERVERS=`cat servers.config`

if [ "X"$1 == "X" ] ; then
    echo "You MUST set a destination on the servers; typically it's \$HOME, so you can use '.' here."
    exit 1
else
    DESTINATION=$1
fi

for i in $APPSERVERS
{
echo "Working on updating app server $i (in $DESTINATION)"
echo " -> Pulling in changes from git, compiling and hot-code reloading the code."
#ssh rack@$i.openforum.rackspace.corp "(if [ -d "$DESTINATION" ] ; then (cd $DESTINATION && git pull && ./rebar compile > /dev/null && ./init.sh reload) else echo "No such directory '$DESTINATION'"; fi )"

ssh rack@$i.openforum.rackspace.corp "(cd openforum && ./gitupdate.sh)" 


# ssh rack@$i.openforum.rackspace.corp "(cd $DESTINATION && rm -rf openforum && git clone git@repoman.rackspace.com:openforum.git && cd openforum && bash sethostname.sh)";
echo " Done with app server $i"
echo ""
}

