jQuery(document).ready(function() {
    $('.search-query').keypress(function(event){
    	if( event.which == 13 ) {
    	    var searchterm = $(this).val();
    	    event.preventDefault();
    	    $.ajax({
    		url: "/search/all_search/" + searchterm,
    		type: "get",
    		success: function(data) {
    		    $('#main-section').slideUp();
    		    $('.result').html(data);
    		    $('.result').slideDown();
    		},
    		error: function(xhr, status, error){
    		    alert("Something blew up: " + status + " Error: " + error);
    		}
    	    });
    	}
    });

    $('.unbookmark_btn').click(function(){
    	$.ajax({
    	    url: "/main/bookmark/" + $(this).attr('subject'),
    	    type: "delete",
    	    complete: function(xhr,status){
    		location.reload();
    	    }
    	})
    });
    $('#sorted-table').dataTable( {
	"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
	"sPaginationType": "bootstrap",
	"oLanguage": {
	    "sLengthMenu": "_MENU_ categories per page"},
    });
});
