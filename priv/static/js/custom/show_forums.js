jQuery(document).ready(function() {
    $(function() {
        // setTimeout() function will be fired after page is loaded                                                                                                                                                                                                           
        // it will wait for 5 sec. and then will fire                                                                                                                                                                                                                         
        // $("#successMessage").hide() function                                                                                                                                                                                                                               
        setTimeout(function() {
            $(".alert").hide('blind', {}, 500)
        }, 3500);
    });
    // Closing a thread so that no posts can be added
    // but it can still be voted on.


    $('.close_thread_btn').click(function(){
    	$.ajax({
    	    url: "/main/close_thread/" + $(this).attr('id'),
    	    type: "put",
    	    complete: function(xhr,status){
    		location.reload();
    	    }
    	})
    });

    $('.reopen_thread_btn').click(function(){
    	$.ajax({
    	    url: "/main/reopen_thread/" + $(this).attr('id'),
    	    type: "put",
    	    complete: function(xhr,status){
    		location.reload();
    	    }
    	})
    });

    // // Bookmark, opposite of below
    // $('.bookmark_btn').click(function(){
    //     $.ajax({
    //         url: "/main/bookmark/" + $(this).attr('subject'),
    //         type: "put",
    //         complete: function(xhr,status){
    //             location.reload();
    //         }
    //     })
    // });

    // // Remove Bookmark, opposite of above
    // $('.unbookmark_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/bookmark/" + $(this).attr('subject'),
    // 	    type: "delete",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });

    // Follow, opposite of below
    // $('.follow_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/follow/" + $(this).attr('subject'),
    // 	    type: "put",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });
    
    // // Unfollow, opposite of above
    // $('.unfollow_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/follow/" + $(this).attr('subject'),
    // 	    type: "delete",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });
    
    // Deleting a thread
    $('.delete_thread_btn').click(function(){
    	$.ajax({
    	    url: "/main/remove_thread/" + $(this).attr('id'),
    	    type: "delete",
    	    complete: function(xhr,status){
    		location.reload();
    	    }
    	})
    });
    // $('#sorted-table').dataTable( {
    //     "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    //     "sPaginationType": "bootstrap",
    //     "oLanguage": {
    //         "sLengthMenu": "_MENU_ categories per page"},
    //     "aaSortingFixed": [[ 0, 'asc' ]],
    //     "iDisplayLength": perpageid,
    //     "aoColumns": [
    //         null,
    //         { "bSortable": false },
    //         { "bSortable": false }
    //     ],
    // });
    // $('#post-sorted-table').dataTable( {
    //     "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    //     "sPaginationType": "bootstrap",
    //     "oLanguage": {
    //         "sLengthMenu": "_MENU_ categories per page"},
    //     "aaSortingFixed": [[ 2, 'desc' ]],
    //     "iDisplayLength": perpageid,
    //     "aoColumns": [
    //         { "bSortable": false },
    //         { "bSortable": false },
    //         null
    //     ],
    // });
});

