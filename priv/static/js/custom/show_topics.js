jQuery(document).ready(function() {
    $(function() {
        // setTimeout() function will be fired after page is loaded                                                                                                                                                                                                           
        // it will wait for 5 sec. and then will fire                                                                                                                                                                                                                         
        // $("#successMessage").hide() function                                                                                                                                                                                                                               
        setTimeout(function() {
            $(".alert").hide('blind', {}, 500)
        }, 3500);
    });
    // We COULD reopen a thread from here, too. ...
    // just thinking ... or we could just show a message
    // somewhere on the page ... "THREAD IS CLOSED, GET IT?!"
    // Closing a thread so that no posts can be added
    // but it can still be voted on.
    $('.close_thread_btn').click(function(){
    	$.ajax({
    	    url: "/main/close_thread/" + $(this).attr('id'),
    	    type: "put",
    	    complete: function(xhr,status){
    		location.reload();
    	    }
    	})
    });

    // Re-open a closed Discussion (=Thread)
    $('.reopen_thread_btn').click(function(){
    	$.ajax({
    	    url: "/main/reopen_thread/" + $(this).attr('id'),
    	    type: "put",
    	    complete: function(xhr,status){
    		location.reload();
    	    }
    	})
    });
    

    // Vote up thread
    $('.vote_up_thread_btn').tooltip('hide');
    $('.vote_up_thread_btn').click(function(){
	$.ajax({
	    url: "/main/vote/" + $(this).attr('owner') + "/" + $(this).attr('id'),
	    type: "put",
	    complete: function(xhr,status){
		location.reload();
	    }
	})
    });
    
    // Vote down, opposite of above function
    $('.vote_down_thread_btn').tooltip('hide');
    $('.vote_down_thread_btn').click(function(){
	$.ajax({
	    url: "/main/vote/" + $(this).attr('owner') + "/" + $(this).attr('id'),
	    type: "delete",
	    complete: function(xhr,status){
		location.reload();
	    }
	})
    });

    // Vote up topic
    $('.vote_up_btn').tooltip({placement:'bottom'});

    $('.vote_up_btn').click(function(){
	$.ajax({
	    url: "/main/vote/" + $(this).attr('owner') + "/" + $(this).attr('id'),
	    type: "put",
	    complete: function(xhr,status){
		location.reload();
	    }
	})
    });
    
    // Vote down, opposite of above function
    $('.vote_down_btn').tooltip({placement:'bottom'});
    $('.vote_down_btn').click(function(){
	$.ajax({
	    url: "/main/vote/" + $(this).attr('owner') + "/" + $(this).attr('id'),
	    type: "delete",
	    complete: function(xhr,status){
		location.reload();
	    }
	})
    });
    
    // Follow function.  Click the button with the ID "#follow_btn" set to follow
    $('.follow_btn').click(function(){
	$.ajax({
	    url: "/main/follow/" + $(this).attr('subject'),
	    type: "put",
	    complete: function(xhr,status){
		location.reload();
	    }
	})
    });
    
    // Unfollow, opposite of above
    $('.unfollow_btn').click(function(){
	$.ajax({
	    url: "/main/follow/" + $(this).attr('subject'),
	    type: "delete",
	    complete: function(xhr,status){
		location.reload();
	    }
	})
    });
    
    // Delete a post after asking the user if it is OK to be deleted
    $('.delete_post_btn').click(function(){
	if( confirm("You sure y'all want to delete that theng?") ) {
	    $.ajax({
		url: "/main/remove_topic/" + $(this).attr('id'),
		type: "delete",
		complete: function(xhr,status){
		    alert("Status returned: " + status);
		    location.reload();
		}
	    })
	} else {
	    alert("You chickened out!");
	}
    });
    
    // The following two functions toggle the "Add" and "Hide" buttons
    $('#add_btn').click(function(){
	$('#add_btn').toggle();
	$('#hide_btn').toggle()
	$('#add_topic_container').toggle();
    });
    
    // Are these two still used? if not ... bye bye
    $('#hide_btn').click(function(){
	$('#add_btn').toggle();
	$('#hide_btn').toggle();
	$('#add_topic_container').toggle();
    });
    
    // This function waits for a click on a button with the CLASS 'solved_button' and then
    // sends an AJAX call to the controller, which then modifies the database for the
    // right state
    $('.solved_button').tooltip({placement:'bottom'});
    $('.solved_button').click(function(){
	$.ajax({
	    url: "/main/solved/" + $(this).val(),
	    type: "put",
	    complete: function(xhr,status){
		location.reload();
	    }
	});
    });

    // Rejects the "solved" state abck to unresolved
    // What happened to these guys? I'd like to have 'em back :)
    $('.reject_button').tooltip({placement:'bottom'});
    $('.reject_button').click(function(){
	$.ajax({
	    url: "/main/solved/" + $(this).val(),
	    type: "delete",
	    complete: function(xhr,status){
		location.reload();
	    }
	});
      });
    
    $('#new_topic_form').submit(function(){
	// This is the "cleanest" way to get text from TinyMCE's patched textarea (aka. The Editor)
	if( tinymce.activeEditor.getContent() == "" ) {
	    alert("Please enter some text!");
	    $('#body').focus();
	    return false;
	}
	return true;
    });

    // Editing a DISCUSSION header
    // Add editor thingy to edit discussion header, then save it.
    // $('#edit_topic_form').submit(function(){
    // 	alert("working");
    // 	$.ajax({
    // 	    url: "/main/fetch_topic_body/" + $(this).attr('topic_id'),
    // 	    type: "get",
    // 	    success: function(data,xhr){
    // 		var text = '#pre_' + $(this).attr('topic_id');
    // 		alert("blah " + text);
    // 		$(text).html(data);
    // 	    }
    // 	});
    // });	    
});
