jQuery(document).ready(function() {
    $(function() {
        // setTimeout() function will be fired after page is loaded                                                                                                                                                                                                           
        // it will wait for 5 sec. and then will fire                                                                                                                                                                                                                         
        // $("#successMessage").hide() function                                                                                                                                                                                                                               
        setTimeout(function() {
            $(".alert").hide('blind', {}, 500)
        }, 3500);
    });

    // Closing a thread so that no posts can be added
    // but it can still be voted on.
    $('.close_thread_btn').click(function(){
    	$.ajax({
    	    url: "/main/close_thread/" + $(this).attr('id'),
    	    type: "put",
    	    complete: function(xhr,status){
    		location.reload();
    	    }
    	})
    });
    
    $('.reopen_thread_btn').click(function(){
    	$.ajax({
    	    url: "/main/reopen_thread/" + $(this).attr('id'),
    	    type: "put",
    	    complete: function(xhr,status){
    		location.reload();
    	    }
    	})
    });
    

    // // Bookmark, opposite of below
    // $('.bookmark_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/bookmark/" + $(this).attr('subject'),
    // 	    type: "put",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });
    
    // // Remove Bookmark, opposite of above
    // $('.unbookmark_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/bookmark/" + $(this).attr('subject'),
    // 	    type: "delete",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });

    // // Follow, opposite of below
    // $('.follow_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/follow/" + $(this).attr('subject'),
    // 	    type: "put",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });
    
    // // Unfollow, opposite of above
    // $('.unfollow_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/follow/" + $(this).attr('subject'),
    // 	    type: "delete",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });

    // Vote up
    // $('.vote_up_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/vote/" + $(this).attr('owner') + "/" + $(this).attr('id'),
    // 	    type: "put",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });
    
    // // Vote down, opposite of above function
    // $('.vote_down_btn').click(function(){
    // 	$.ajax({
    // 	    url: "/main/vote/" + $(this).attr('owner') + "/" + $(this).attr('id'),
    // 	    type: "delete",
    // 	    complete: function(xhr,status){
    // 		location.reload();
    // 	    }
    // 	})
    // });
    
    // Deleting a thread
    $('.delete_thread_btn').click(function(){
	if( confirm("Are you sure to delete\n\n\tAND LOOSE ALL DATA\n\nof this thread?\nThink twice!") ) {
    	    $.ajax({
    		url: "/main/remove_thread/" + $(this).attr('id'),
    		type: "delete",
    		complete: function(xhr,status){
    		    location.reload();
    		}
    	    })
	} else {
	    alert("Thread not deleted.");
	}
    });

    $('#new_thread_form').submit(function(){	  
	// Checking the thread name
	if(!$('#thread_name').val()) {
	    alert("Please enter a name for the discussion.");
	    $('#thread_name').focus();
	    return false;
	}
	
	// This is the "cleanest" way to get text from TinyMCE's patched textarea (aka. The Editor)
	if( tinymce.activeEditor.getContent() == "" ) {
	    alert("Please enter some text!");
	    $('#body.tinymce.mcecontentbody').focus();
	    return false;
	}

	// Checking Tags, no saving w/o tags
	if(!$('#tags').val()) {
	    alert("Please enter a tag or multiple tags separated by commas.");
	    $('#tags').focus();
	    return false;
	}
	
	return true;
      });

    $('#new_thread_form').submit(function(){
	return true;
    });
    $('.icon-star').tooltip('hide');
    $('.vote_up_btn').click(function(){
        $.ajax({
            url: "/main/vote/" + $(this).attr('owner') + "/" + $(this).attr('id'),
            type: "put",
            complete: function(xhr,status){
                window.location.reload();
            }
        })
    });
    
    // // Vote down, opposite of above function                                                                                                                                                                                         
    $('.vote_down_btn').click(function(){
    	$.ajax({
            url: "/main/vote/" + $(this).attr('owner') + "/" + $(this).attr('id'),
            type: "delete",
            complete: function(xhr,status){
    		window.location.reload();
            }
        })
    });
});

