function get_notifications() {
    $.ajax({
        url: "/main/notification/" + "{{owner.id}}",
        type: "get",
        async: true,
        timeout: 50000,
        success: function(data,xhr){
            msg = data.messages;
            $('#notify').html("<li><a>" + msg + "</a></li>");
            setTimeout(
                get_notifications,
                25000
            );
        },
        error: function(xhr,status){
            console.log("no notifications");
            setTimeout(
                get_notifications, /* Try again after.. */
                15000); /* milliseconds (15seconds) */
        }
    });
};

$(document).ready(function() {
    get_notifications();
    
    $('.search-query').keypress(function(event){
	if( event.which == 13 ) {
	    var searchterm = $(this).val();
	    event.preventDefault();
	    $.ajax({
		url: "/search/all_search/" + searchterm,
		type: "get",
		success: function(data) {
		    $('#main-section').slideUp();
		    $('.result').html(data);
		    $('.result').slideDown();
		},
		error: function(xhr, status, error){
		    alert("Something blew up: " + status + " Error: " + error);
		}
	    });
	}
    });
});
