jQuery(document).ready(function() {
    $('#login_form').submit(function(){
	if( !$('#sso_username').val() ) {
	    alert("You'll need to provide your SSO username.");
	    $('#sso_uswername').focus();
	    return false;
	}
	
	if(!$('#sso_password').val() ) {
	    alert("Please provide your SSO password to logon.");
	    $('#sso_password').focus();
	    return false;
	}
	return true;
    });
});
