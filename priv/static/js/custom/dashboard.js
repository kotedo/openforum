jQuery(document).ready(function() {
    $(function() {
        // setTimeout() function will be fired after page is loaded                                                                                                                                                                           
        // it will wait for 5 sec. and then will fire                                                                                                                                                                                         
        // $("#successMessage").hide() function                                                                                                                                                                                               
        setTimeout(function() {
            $(".alert").hide('blind', {}, 500)
        }, 3500);
    });
    $('.create_forum_btn').click(function() {
	$('.container_forum_create').toggle('blind', {}, 500);
    });

    $('.delete_forum').tooltip('hide');
    $('#forums .delete_forum').click(function(){
	if( confirm("Are you sure you want to delete the Forum?\n\nAll Posts and Discussions  will be lost!!") ) {
	    $.ajax({
		url: "/main/new_delete/" + $(this).attr('object'),
		type: "delete",
		complete: function(){
		    $('#forums').load('/dashboard #forums');
		}
	    });
	} else { 
	    return false;
	}
    });

    // $('.change_user_level').click(function() {
    //     $.ajax({
    //         url: "/main/change_user_level/" + $(this).val() + "/" + $('.change_user_level').attr('owner'),
    //         type: "put"
    //     });
    // });

    // $('.delete_channel').click(function(){
    // 	if( confirm("Are you sure you want to delete the Channel?\n\nAll Forums, Posts and Discussions  will be lost!!") ) {
    // 	    return true;
    // 	} else { 
    // 	    return false;
    // 	}
    // })
 
    // This is for the datatable
    // $("#user-sorted-table tbody tr td a.promote_btn").tooltip('hide');
    // $("#user-sorted-table tbody tr td a.promote_btn").click(function(){
    // 	$.ajax({
    // 	    url: "/main/permissions/" + $(this).attr('object'),
    // 	    type: "put",
    // 	    complete: function(){
    // 		location.reload();
    // 	    }
    // 	})
    // });
    // $("#user-sorted-table tbody td a.demote_btn").tooltip('hide');
    // $("#user-sorted-table tbody td a.demote_btn").click(function(){
    // 	$.ajax({
    // 	    url: "/main/permissions/" + $(this).attr('object'),
    // 	    type: "delete",
    // 	    complete: function(){
    // 		location.reload();
    // 	    }
    // 	})
    // });

    // $("#example tbody").on('click', 'td.delete', function(event) {
    // 	var row = $(this).closest("tr").get(0);
    //     oTable.fnDeleteRow( row );
    // });
    // // Promoting an owner (User)
    $('.promote_btn').tooltip('hide');
    $('.promote_btn').click(function(){
    	$.ajax({
    	    url: "/main/permissions/" + $(this).attr('object'),
    	    type: "put",
            complete: function(){                                                                                                                                                                                                         
                $('#forums').load('/dashboard #forums');                                                                                                                                                                                  
            }
    	})
    });
    $('#channels .delete_forum').click(function(){
	if( confirm("Are you sure you want to delete the Channel?\n\nAll Forums, Posts and Discussions  will be lost!!") ) {
    	    $.ajax({
    		url: "/main/new_delete/" + $(this).attr('object'),
    		type: "delete",
    		complete: function(){
    		    $('#channels').load('/dashboard #channels');
		    $('#forums').load('/dashboard #forums');
    		}
    	    });
	} else { 
	    return false;
	}
    });
    
    // // Demoting an owner (User)
    $('.demote_btn').tooltip('hide');
    $('.demote_btn').click(function(){
    	$.ajax({
    	    url: "/main/permissions/" + $(this).attr('object'),
    	    type: "delete",
    	    complete: function(){
    		location.reload();
    	    }
    	})
    });    

    $('.Remove_glossary_btn').click(function(){
    	if( confirm("Are you sure to delete this glossary term?") ) {
    	    $.ajax({
    		url: "/glossary/delete/" + $(this).attr('id'),
    		type: "delete"
    	    })
    	}
    });

    $('.add_glossary_btn').click(function(){
    	$('.add_glossary_btn').hide();
    	$('.hide_glossary_btn').show();
    	$('.add_glossary_editor').show();
    	$('#term').focus();
    });

    $('.hide_glossary_btn').click(function(){
    	$('.add_glossary_btn').show();
    	$('.hide_glossary_btn').hide();
    	$('.add_glossary_editor').hide();
    });


    $('#add_glossary_form').submit(function(){
    	var term  = $(this).attr('term');
    	var description = $(this).attr('description');

    	alert("Term: '" + term + "'\n" + "Description:\n" + description);

    	return false;
    })
    $('.show_tooltip').tooltip('hide');
    // $('select.change_user_level').change(function() {
    //     $.ajax({
    //         url: "/main/change_user_level/" + $(this).val(),
    //         type: "put"
    //     });
    // });
});
