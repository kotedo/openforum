jQuery(document).ready(function() {
    $(function() {
        // setTimeout() function will be fired after page is loaded                                                                                                                                                                                                           
        // it will wait for 5 sec. and then will fire                                                                                                                                                                                                                         
        // $("#successMessage").hide() function                                                                                                                                                                                                                               
        setTimeout(function() {
            $(".alert").hide('blind', {}, 500)
        }, 3500);
    });
    $('.frequency_auto').click(function() {
    	$.ajax({
    	    url: "/main/frequency/" + $(this).attr('subscription') + "/" + "1",
    	    type: "put"
    	});
    });
    $('.frequency_daily').click(function() {
    	$.ajax({
    	    url: "/main/frequency/" + $(this).attr('subscription') + "/" + "2",
    	    type: "put"
    	});
    });
    $('.frequency_weekly').click(function() {
    	$.ajax({
    	    url: "/main/frequency/" + $(this).attr('subscription') + "/" + "3",
    	    type: "put"
    	});
    });

    $('#per_page').click(function() {
	$.ajax({
	    url: "/main/perpage/" + $('#per_page').val(),
	    type: "put"
	});
    });
    


    $('.unbookmark_btn').click(function(){
    	$.ajax({
    	    url: "/main/bookmark/" + $(this).attr('subject'),
    	    type: "delete",
    	    complete: function(xhr,status){
		var oTable = $('#bookmark-sorted-table').dataTable();
		oTable.fnDeleteRow( 0 );
    	    }
    	});
    });
    
    $('#locale').click(function() {
    	$.ajax({
    	    url: "/main/locale/" + $(this).val(),
    	    type: "put"
    	});
    });
    
    $('#back_to_userimage').click(function() {
    	alert("Reset Avatar: " + $(this).val());
    	$.ajax({
    	    url: "/main/avatar/" + $('#back_to_userimage').val(),
    	    type: "delete"
    	});
    });
    
    $('.unfollow_btn').tooltip('hide');
    $('#bookmark-sorted-table .unfollow_btn').click(function(){
    	bTable.fnDeleteRow(this.parentNode.parentNode);
 	$.ajax({
    	    url: "/main/follow/" + $(this).attr('subject'),
    	    type: "delete"
    	})
    });
    $('#subscription-sorted-table .unfollow_btn').click(function(){
    	sTable.fnDeleteRow(this.parentNode.parentNode);
 	$.ajax({
    	    url: "/main/follow/" + $(this).attr('subject'),
    	    type: "delete"
    	})
    });
});
