jQuery(document).ready(function() {
    $(function() {
        // setTimeout() function will be fired after page is loaded                                                                                                                                                                                                           
        // it will wait for 5 sec. and then will fire                                                                                                                                                                                                                         
        // $("#successMessage").hide() function                                                                                                                                                                                                                               
        setTimeout(function() {
            $(".alert").hide('blind', {}, 500)
        }, 3500);
    });
    $('.timeago').timeago();

    // Working search function for the navigation bar on the main page ...
    $('.search-query').keypress(function(event){
    	  if( event.which == 13 ) {
    	      var searchterm = $(this).val();
    	      event.preventDefault();

            $.ajax({
    		        url: "/search/all_search/" + searchterm,
    		        type: "get",
    		        success: function(data) {
    		            $('#main-section').slideUp();
    		            $('.result').html(data);
    		            $('.result').slideDown();
    		        },
    		        error: function(xhr, status, error){
    		            alert("Something blew up: " + status + " Error: " + error);
    		        }
    	      });
    	  }
    });
    
    $('#post-sorted-table').dataTable( {
    	"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    	"sPaginationType": "bootstrap",
    	"oLanguage": {
    	    "sLengthMenu": "_MENU_ per page"},
    	"aaSorting": [[ 1, 'asc' ]],
    	"iDisplayLength": 10,
    });

    $('#bookmark-sorted-table').dataTable( {
    	"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    	"sPaginationType": "bootstrap",
    	"oLanguage": {
    	    "sLengthMenu": "_MENU_ per page"},
    	"aaSorting": [[ 1, 'desc' ]],
    	"iDisplayLength": 10,
	"aoColumns": [
            null,
            { "bVisible": false,
              "sType": "date"},
            { "iDataSort": 1}
        ]
    });
    
});
