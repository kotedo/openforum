function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';'); for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function get_env(){
    if( window.location.host.substring(0,9) == "localhost" ){
        return "dev";
    } else {
        return "prod";
    }
}

function base_utag_data() {
    return {
        "page_name":document.title,
        "rack_id":readCookie('IS_UASrackuid'),
        "session_id":readCookie('RackSID'),
        "site_language":"en",
        "icmp": get_env()
    };
}

var utag_data = base_utag_data();

(function(a,b,c,d){
    a='//tags.tiqcdn.com/utag/rackspace/rpcforum/prod/utag.js';
    b=document;
    c='script';
    d=b.createElement(c);
    d.src=a;
    d.type='text/java'+c;
    d.async=true;
    a=b.getElementsByTagName(c)[0];
    a.parentNode.insertBefore(d,a);
})();
