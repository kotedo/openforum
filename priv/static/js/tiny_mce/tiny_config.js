// Configuration file tiny_mce
// Adjust to your linkings

tinyMCE.init({
    // General options
    mode : "textareas",
    theme : "advanced",
    skin : "bootstrap",
    plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

    // Theme options
    theme_advanced_buttons1 : "justifyleft,justifycenter,justifyright,justifyfull,|,bold,italic,underline,strikethrough,|,sub,sup,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,mediacode,|,preview,|,fullscreen",
    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,insertdate,inserttime,|,charmap,emotions,iespell,advhr",
    theme_advanced_buttons4 : "visualchars,nonbreakingpagebreak,template,|,cleanup,|,search,replace,|,print,help",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    
    // Example content CSS (should be your site CSS)
    content_css : "/static/css/DT_bootstrap.css",
    
    // Drop lists for link/image/media/template dialogs
    // Correct
    //template_external_list_url : "js/template_list.js",
    
    // Works
    //external_link_list_url : "/static/js/custom/external_link_list.js",

    // Works as a JavaScript file with STATIC information in it.
    // external_image_list_url : "/static/js/custom/external_image_list.js",

    // Might work with DYNAMIC information from the image table ... DB call
    external_image_list_url : "/tiny/external_image_list",

    //media_external_list_url : "js/media_list.js",
    
    // Replace values for the template plugin
    template_replace_values : {
        username : "Some User",
        staffid : "991234"
    }
});
