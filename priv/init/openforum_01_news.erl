-module(openforum_01_news).
-export([init/0, stop/1]).

-include("../../include/permissions.hrl").

% This script is first executed at server startup and should
% return a list of WatchIDs that should be cancelled in the stop
% function below (stop is executed if the script is ever reloaded).
init() ->
    %% Not the best way, but let's hope that's legit ...
    case boss_db:count(owner, []) of
	0 ->
	    error_logger:info_msg("There are no users. -> Seeding DB with admin user~n", []),
	    seed_db:add_admin_group(),
	    seed_db:add_admin_account(),
	    seed_db:setup_strengths();
	Data ->
	    error_logger:info_msg("Database ok.~n", [])
    end,

    %% Starting CloudFiles as an OTP addon
    %%ok = application:start(cloudfiles),
    %%ok = cloudfiles:authenticate(),
    
    {ok, ForumsWatch} = boss_news:watch("forums",
					fun
					    (created, NewForum) ->
						ChannelId = NewForum:channel_id(),
						Channel = boss_db:find(ChannelId),
						Update = Channel:set([{updated_on, erlang:now()}]),
						Update:save(),
						notify_subscribers([{forum, NewForum},{channel, Channel}]);
					    (deleted, OldForum) ->
						ok
					end),

    {ok, ThreadsWatch} = boss_news:watch("threads",
					fun
					    (created, NewThread) ->
						ForumId = NewThread:forum_id(),
						Forum = boss_db:find(ForumId),
						Update = Forum:set([{updated_on, erlang:now()}]),
						Update:save();
					    (deleted, OldThread) ->
						ok
					end),


   {ok, TopicsWatch} = boss_news:watch("topics",
				       fun
					   (created, NewTopic) ->
					       ThreadId = NewTopic:thread_id(),
					       Thread = boss_db:find(ThreadId),
					       Update = Thread:set([{updated_on, erlang:now()}]),
					       Update:save(),
					       %%ForumId = Thread:forum_id(),
					       %%Forum = boss_db:find(ForumId),
					       %%ChannelId = ForumId:channel_id(),
					       %% Step one, finding all Owners that have a subscription     
					       Owners = lists:map(fun(X) -> X:owner_id() end, boss_db:find(subscription, [{object, ThreadId}])),
					       %% Owners = lists:map(fun(X) -> boss_db:find(owner, [X]) end,
					       %% Step two, notify them
					       case Owners of
						   undefined -> 
                                                       error_logger:info_msg("No Subscribers");
						   Subscribers ->
						       %% Original = {thread, [{id, Thread:id()},{name, Thread:name()}]},
						       %% ThreadInfo = [ {X,iolist_to_binary(Y)} || {X,Y} <- Original ],
						       ThreadId_mod = iolist_to_binary(Thread:id()),
						       ThreadName_mod = iolist_to_binary(Thread:name()),
						       notify_list(Owners, ThreadId_mod, ThreadName_mod)
						   end;
					   (deleted, OldTopic) ->
					       ok
				       end),

    %% This guy here watches over threads being closed or reopened,
    %% feel free to add boss_mq:push() messages and/or outgoing emails
    %% 04/26/2013 00:07:33: ThreadWatch close/re-open event added as of PivotalTracker ID #48786217
    {ok, ThreadWatch} = boss_news:watch("thread-*.open",
					fun 
					    (updated, {Thread, 'open', false, true}) ->
						error_logger:info_msg("Thread:open was FALSE is now TRUE: ~p~n", [Thread]);
					    (updated, {Thread, 'open', true, false}) ->
						error_logger:info_msg("Thread:open was TRUE is now FALSE: ~p~n", [Thread])
					end),

    %% 04/26/2013 00:08:10: Trigger for CurationProcess added as of PivotalTracker ID #48786209
    {ok, SolvedWatch} = boss_news:watch("topic-*.solved",
					fun
					    (updated, {Topic, 'solved', false, true}) ->
						error_logger:info_msg("Hurray!  We have a solution to a discussion! : ~p~n", [Topic]),
						%% Try to communicate to our OpenBridge OTP app to migrate the topic into an Article;
						%% Fail gracefully if nobody is listening.
						Request = lists:flatten(io_lib:format("http://localhost:3000/OpenForum/~s/OpenKB", [Topic:id()])),
						try httpc:request(put, {Request,[],[],[]}, [], [{sync, true}]) of
						    {ok, {_,_,Status}} ->
							error_logger:info_msg("Sent to OpenBridge, status: ~p~n", [Status]),
							ok;
						    _ ->
							
							ok
						catch
						    Error:Reason ->
							{Error, Reason}
						end;
					    (updated, {Topic, 'solved', true, false}) ->
						error_logger:info_msg("Awwww!  We thought we had a solution to a discussion ... but no : ~p~n", [Topic]),
						%% Try to communicate to our OpenBridge OTP app to remove the topic from the Queue
						%% since someone decided it's not a potential OpenKB article
						%% Fail gracefully if nobody is listening
						Request = lists:flatten(io_lib:format("http://localhost:3000/OpenForum/~s/OpenKB", [Topic:id()])),
						try httpc:request(delete, {Request,[]}, [], [{sync, true}]) of
						    {ok, {_,_,Status}} ->
							error_logger:info_msg("Sent remove request to OpenBridge, status: ~p~n", [Status]),
							ok;
						    _ ->
							ok
						catch
						    Error:Reason ->
							{Error, Reason}
						end
					end),

    {ok, [ForumsWatch, ThreadsWatch,
	  TopicsWatch, ThreadWatch,
	  SolvedWatch]}.

notify_list(Owners, ThreadId_mod, ThreadName_mod) when is_list(Owners) -> 
    lists:map(fun(X) -> 
		       error_logger:info_msg(" ~p was ~p~n",
					     [ThreadId_mod, ThreadName_mod]),
		      boss_mq:push(X, {thread, [{id, ThreadId_mod},{name, ThreadName_mod}]}) end, Owners).


stop(ListOfWatchIDs) ->
    lists:map(fun boss_news:cancel_watch/1, ListOfWatchIDs).



notify_subscribers([{forum, ForumData}, {channel, Channel}]) ->
    %% Find all subscribers that match the channel thay subscribed to
    %% Subscribers are basically Owners, just don't want to use Owner here
    %% might be too confusing later on.
    ChannelName = Channel:name(),
    ForumComment = ForumData:comments(),
    ForumName = ForumData:name(),
    OwnerName = (boss_db:find(ForumData:owner_id())):fullname(),
    Subscribers = boss_db:find(subscription, [{object, ForumData:channel_id()}]),

    Title = lists:flatten(io_lib:format("~s created a new category, named '~s'.", [OwnerName, ForumName])),
    Body = lists:flatten(io_lib:format("Hello,~n~n~p~n~nClick here: http://localhost:8001/forum/show_threads/~s",[Title, ForumData:id()])),
    
    Emails = [ boss_mail:send( "openforum@rackspace.com", binary_to_list((boss_db:find(X:owner_id())):email()), Title, Body) || X <- Subscribers ],
    error_logger:info_msg("FORUMDATA   : ~p~n"
			  "SUBSCRIBERS : ~p~n"
			  "EMAILS      : ~p~n"
			  "CHANNELNAME : ~p~n"
			  "COMMENT     : ~p~n"
			  "FORUMNAME   : ~p~n"
			  "OWNERNAME   : ~p~n", [ForumData, Subscribers, Emails,
						 ChannelName, ForumComment, ForumName, OwnerName]),
    ok.



%%%%%%%%%%% Ideas
%    boss_news:watch("user-42.*",
%        fun
%            (updated, {Donald, 'location', OldLocation, NewLocation}) ->
%                ;
%            (updated, {Donald, 'email_address', OldEmail, NewEmail})
%        end),
%
%    boss_news:watch("user-*.status",
%        fun(updated, {User, 'status', OldStatus, NewStatus}) ->
%                Followers = User:followers(),
%                lists:map(fun(Follower) ->
%                            Follower:notify_status_update(User, NewStatus)
%                    end, Followers)
%        end),
%
%    boss_news:watch("users",
%        fun
%            (created, NewUser) ->
%                boss_mail:send(?WEBSITE_EMAIL_ADDRESS,
%                    ?ADMINISTRATOR_EMAIL_ADDRESS,
%                    "New account!",
%                    "~p just created an account!~n",
%                    [NewUser:name()]);
%            (deleted, OldUser) ->
%                ok
%        end),
%    
%    boss_news:watch("forum_replies",
%        fun
%            (created, Reply) ->
%                OrignalPost = Reply:original_post(),
%                OriginalAuthor = OriginalPost:author(),
%                case OriginalAuthor:is_online() of
%                    true ->
%                        boss_mq:push(OriginalAuthor:comet_channel(), <<"Someone replied!">>);
%                    false ->
%                        case OriginalAuthor:likes_email() of
%                            true ->
%                                boss_mail:send("website@blahblahblah",
%                                    OriginalAuthor:email_address(),
%                                    "Someone replied!"
%                                    "~p has replied to your post on ~p~n",
%                                    [(Reply:author()):name(), OriginalPost:title()]);
%                            false ->
%                                ok
%                        end
%                end;
%            (_, _) -> ok
%        end),
%    
%    boss_news:watch("forum_categories",
%        fun
%            (created, NewCategory) ->
%                boss_mail:send(?WEBSITE_EMAIL_ADDRESS,
%                    ?ADMINISTRATOR_EMAIL_ADDRESS,
%                    "New category: "++NewCategory:name(),
%                    "~p has created a new forum category called \"~p\"~n",
%                    [(NewCategory:created_by()):name(), NewCategory:name()]);
%            (_, _) -> ok
%        end),
%
%    boss_news:watch("forum_category-*.is_deleted",
%        fun 
%            (updated, {ForumCategory, 'is_deleted', false, true}) ->
%                ;
%            (updated, {ForumCategory, 'is_deleted', true, false}) ->
%        end).

% Invoking the API directly:
%boss_news:deleted("person-42", OldAttrs),
%boss_news:updated("person-42", OldAttrs, NewAttrs),
%boss_news:created("person-42", NewAttrs)

% Invoking the API via HTTP (with the admin application installed):
% POST /admin/news_api/deleted/person-42
% old[status] = something

% POST /admin/news_api/updated/person-42
% old[status] = blah
% new[status] = barf

% POST /admin/news_api/created/person-42
% new[status] = something
