%% Defintions of Permissions for the different user levels

-define(USER ,  [read]).
-define(EDITOR, [read,create,update]).
-define(CURATOR,[read,create,update,edit]).
-define(ADMIN,  [read,create,update,edit,delete]).
