-module(forum, [Id, ChannelId, OwnerId, Name, Comments, Tags, Oldid, CreatedOn, UpdatedOn]).
-compile(export_all).
-counter(forums_counter).

-belongs_to(channel).
-belongs_to(owner).
-has({threads,many}).
