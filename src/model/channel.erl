-module(channel, [Id, Name, OwnerId, Description, Position, Tags, Userid, Oldid, CreatedOn, UpdatedOn]).
-compile(export_all).
-counter(channel_counter).

-belongs_to(owner).
-has({forums,many}).
