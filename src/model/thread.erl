-module(thread, [Id, ForumId, OwnerId, Name, Comments, Tags, Open, Votes, Oldid, CreatedOn, UpdatedOn]).
-compile(export_all).

-belongs_to(forum).
-belongs_to(owner).

-has({topics,many}).
-counter(thread_id_counter).
