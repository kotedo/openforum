-module(topic, [Id, ThreadId, OwnerId, Name, Tags, Body, Solved, SolvedBy, SolvedOn, Votes, CreatedOn, UpdatedOn]).
-compile(export_all).

-belongs_to(thread).
-belongs_to(owner).
