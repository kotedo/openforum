-module(profile, [Id,
		  OwnerId, Userimage, Avatar, AboutMe,
		  Strength1, Strength2, Strength3,
		  Strength4, Strength5,
		  PerPage, JobTitle, Locale,
		  %% 04/10/2013 15:49:23: Signature added as of PivotalTracker ID #47819975
		  Signature,
		  CreatedOn, UpdatedOn]).

-compile(export_all).

-belongs_to(owner).

all_strengths() ->
    [Strength1,
     Strength2,
     Strength3,
     Strength4,
     Strength5].

has_strength(Strength) ->
    lists:member(Strength, all_strengths()).
