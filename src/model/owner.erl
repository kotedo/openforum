-module(owner, [Id,Firstname,Lastname,Email,LdapUid,Permissions,Groups,Active,Userid,CreatedOn,UpdatedOn]).
-compile(export_all).

-has({subscriptions,many}).
-has({profile,1}).
-has({bookmarks,many}).

-include("../../include/permissions.hrl").

fullname() ->
    lists:flatten(io_lib:format("~s ~s", [binary_to_list(Firstname), binary_to_list(Lastname)])).

in_group(Group) ->
    lists:member(Group, Groups).

has_permission(Permission) ->
    lists:member(Permission, Permissions).

is_admin() ->
    case Permissions of
	?ADMIN ->
	    true;
	_ ->
	    false
    end.

%%============================================
%%  These validation tests were throwing errors when trying to promote/ demote users:
%% {badarg,[{erlang,length,[<<"Pat">>],[]},{owner,'-validation_tests/1-fun-0-',1,[{file,"/Users/lanefielder/Documents/repo/openforum/src/model/owner.erl"},{line,32}]}
%%
%%  I removed the / between the == and that resolved it. Trying to find documentation on the correct syntax for these validation tests.
%%===============================================

%% -------------------------------------------------------------------------------
%%  @doc Turns the Arg into a normal "string" (list) but only when it is a binary
%% -------------------------------------------------------------------------------
tolist(Arg) when is_binary(Arg) ->
    binary_to_list(Arg);
tolist(Arg) ->
    Arg.

validation_tests() ->
    [
     %% Firstname Required
     {fun() ->	      
	      Firstname =/= undefined andalso length(tolist(Firstname)) =/= 0
      end, {firstname, "Required"}},
     %% Lastname Required
     {fun() ->
	      Lastname =/= undefined andalso length(tolist(Lastname)) =/= 0
      end, {lastname, "Required"}}
    ].

%%%============================================================================
%%% Model Hooks
%%%============================================================================
 
%% Set created_at and updated_at on creation
%% before_create() ->    
%%     Now = calendar:now_to_universal_time(erlang:now()),
%%         {ok, set([{created_at, Now}, {updated_at, Now}])}.
 
%% %% Update updated_at on update
%% before_update() ->
%%     {ok, set(updated_at, calendar:now_to_universal_time(erlang:now()))}.

before_delete() ->
    %% Prevent the admin user to be deleted
    case Firstname =:= "Admin" andalso Lastname =:= "Admin" of
	true ->
	    {error, cannot_delete_admin};
	false ->
	    ok
    end.
