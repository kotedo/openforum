-module(announcement, [Id, Name::string(), Description::string(), Order::integer(), Visible, ExpiresOn, CreatedOn, UpdatedOn]).
-compile(export_all).
%% 04/26/2013 00:10:39: Announcement database model added as of PivotalTracker ID #45971969
