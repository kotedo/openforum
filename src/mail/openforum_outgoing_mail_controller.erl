-module(openforum_outgoing_mail_controller).
-compile(export_all).

%% See http://www.chicagoboss.org/api-mail-controller.html for what should go in here

test_message(FromAddress, ToAddress, Subject) ->
    Headers = [
        {"Subject", Subject},
        {"To", ToAddress},
        {"From", FromAddress}
    ],
    {ok, FromAddress, ToAddress, Headers, [{address, ToAddress}]}.

list_message(FromAddress, ToAddress, Subject) ->
    Headers = [
	       {"Subject", Subject},
	       {"To", ToAddress},
	       {"From", FromAddress},
	       {"Cc", "kai.janson@rackspace.com"},
	       {"Bcc", "kotedo@gmail.com;mrisoli@rackspace.com;lane.fielder@rackspace.com"}
	      ],
    {ok, FromAddress, ToAddress, Headers, [{address, ToAddress},{name, "people"}]}.

foo_message(FromAddress, ToAddress, Subject) ->
    Owner = boss_db:find_first(owner, [{email, FromAddress}]),
    Headers = [
	       {"X-Mailer", "OpenForum"},
	       {"X-OpenForum-Host", net_adm:localhost()},
	       {"X-OpenForum-ThreadId", "thread-517a02ae69e58122b2000003"},
	       {"X-OpenForum-ThreadName", "And One More To Test"},
	       {"Subject", Subject},
	       {"To", ToAddress},
	       {"From", FromAddress},
	       {"Reply-To", "OpenForum <reply@openforum.rackspace.corp>"}
	      ],
    {ok, FromAddress, ToAddress, Headers, [{username, Owner:fullname()}]}.

help_message(FromAddress, ToAddress, Subject) ->
    Headers = [
	       {"Subject", Subject},
	       {"To", ToAddress},
	       {"From", FromAddress}
	      ],
    {ok, FromAddress, ToAddress, Headers, [{username, "Racker"}]}.

post_message(FromAddress, ToAddress, Subject) ->
    Headers = [
	       {"Subject", Subject},
	       {"To", ToAddress},
	       {"From", FromAddress}
	      ],
    {ok, FromAddress, ToAddress, Headers, [{username, "Racker"},{topic, Subject}]}.
