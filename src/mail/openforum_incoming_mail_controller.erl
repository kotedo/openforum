-module(openforum_incoming_mail_controller).
-compile(export_all).

%% Can be used to make sure that the poster
%% is a legit, active Racker
authorize_(User, DomainName, IPAddress) ->
    true.

%% TODO: explain
post(FromAddress, {A,B,Headers, Extra, Body}) ->
    case boss_db:find_first(owner, [{email, FromAddress}]) of
	[] ->
	    error_logger:error_msg("FromAddress is not a rackspace email, dropping message: ~p~n", [FromAddress]);
	ValidUser ->
	    Subject = proplists:get_value(<<"Subject">>, Headers, "No Subject"),
	    error_logger:info_msg("VERIFIED INCOMING email:~nFrom: ~p~nUser: ~p~nSubject: ~p~nBody: ~p~n", [FromAddress, ValidUser, Subject, Body]),
	    boss_mail:send_template( openforum, post_message, ["noreply@openforum.rackspace.corp", FromAddress, Subject])
    end,
    ok.

%% TODO: explain
reply(FromAddress, {A,B,Headers,D,[{_,_,_,_, NewPlain},{_,_,_,_, NewHTML}]}) ->
    error_logger:info_msg("Here it is (A):~n~p~n~n", [A]),
    error_logger:info_msg("Here it is (B):~n~p~n~n", [B]),
    error_logger:info_msg("Here it is (C):~n~p~n~n", [Headers]),
    error_logger:info_msg("Here it is (D):~n~p~n~n", [D]),
    error_logger:info_msg("Got an email from ~p and here is the data:~n~nNew: ~s~n~nOld: ~s~n~n~n", [FromAddress, NewPlain, NewHTML]),
    ok.


%% Explains itself
%% TODO: Use the Subject line of the incoming help email to
%% provide help on a topic?
help(FromAddress, Message) ->
    boss_mail:send_template( openforum, help_message, ["noreply@openforum.rackspace.corp", FromAddress, "OpenForum Email Help System"]),
    ok.

%% Explains itself
noreply(_,_) ->
    ok.
