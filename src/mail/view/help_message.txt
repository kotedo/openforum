Hello {{username}},

OpenForum accepts emails for these accounts:

post@openforum.rackspace.corp
	Here you send you discussion proposal to the system.
	Your SUBJECT line becomes the subject line for the Discussion,
	Your Email itself will be the initial post/question of the new discussion.

reply@openforum.rackspace.corp
	Simply hit "Reply" in your email client and that's it.
	Don't modify the Subject line, but feel free to write a good response.

and


help@openforum.rackspace.corp
	(You're looking at this right now)
