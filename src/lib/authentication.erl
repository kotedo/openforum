-module(authentication).
-export([logged_in/2]).

logged_in(SessionID, Req) ->
    case boss_session:get_session_data(SessionID, ldap_uid) of
	undefined ->
	    %% Not logged in, goto login screen
	    {redirect, [{controller, "security"}, {action, "login"}]};
	LoggedIn ->
	    %% Fetches current user
	    {ok, boss_db:find_first(owner, [{ldap_uid, LoggedIn}])}
    end.
