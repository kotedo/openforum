-module(migrate).
-export([
	 create_users/0,
	 create_forums/0,
	 create_threads/0,
	 create_topics/0,
	 rem_owner_dupes/0,
	 list_strengths/0,
	 remove_all_users/0,
	 finduser_safely/1,
	 fix_thread_tags/0,
	 fix_topic_tags/0
	]).

rem_owner_dupes() ->
    lists:map(fun(X) ->
		      case length(boss_db:find(owner, [{userid, X:userid()}])) > 1 of
			  true ->
			      io:format("Removing ~p~n", [X:email()]),
			      boss_db:delete((boss_db:find_last(owner, [{email, X:email()}])):id());
			  _ -> ok
		      end
	      end, boss_db:find(owner, [])).

remove_all_users() ->
    lists:map(fun(X) ->
		      case X:profile() of
			  undefined ->
			      ok;
			  Profile ->
			      boss_db:delete(Profile:id())
		      end,
		      boss_db:delete(X:id())
	      end, boss_db:find(owner, [])).

list_strengths() ->
    lists:map(fun(X) ->
		      try
			  Owner = boss_db:find(X:owner_id()),
			  Fullname = case length(binary_to_list(Owner:firstname())) >0  andalso length(binary_to_list(Owner:lastname())) > 0 of
					 true ->
					     binary_to_list(Owner:firstname()) ++ " " ++ binary_to_list(Owner:lastname());
					 _ ->
					     binary_to_list(Owner:email())
				     end,
			  io:format("Name: ~p S1: ~s, ~s, ~s, ~s, ~s ~n", [ Fullname,
									    X:strength1(), X:strength2(),X:strength3(), X:strength4(), X:strength5()])
		      catch
			  Error:Class ->
			      lager:error("Error: ~p / Class : ~p~n", [Error, Class])
		      end
	      end, boss_db:find(profile, [])).

save_photo(SavedOwner, RawPhoto) ->
    [EmailName,_] = string:tokens(binary_to_list(SavedOwner:email()), "@"),
    Path = lists:flatten(io_lib:format("static/userimages/~s.jpg", [EmailName])),
    file:write_file(filename:join("priv", Path), RawPhoto),
    Path.

%% Vanilla's GDN_Users are becoming an "Owner" with a profile.
%% -module(owner, [Id,Firstname,Lastname,Email,LdapUid,Permissions,Groups,Active,Userid,CreatedOn,UpdatedOn]).
%% -module(gdn_user, [Id, Userid, Name, Password, HashMethod, Photo, About, Email, ShowEmail, Gender, CountVisits, CountInvitations, CountNotifications, InviteUserid,
%%		   DiscoveryText, Preferences, Permissions, Attributes, DateSetInvitations, DateOfBirth, DateFirstVisit, DateLastActive, LastIPAddress,
%%		   DateInserted, InsertIpAddress, DateUpdated, UpdateIpAddress, HourOffset, Score, Admin, Banned, Deleted,
%%		   CountUnreadConversations, CountDiscussions, CountUnreadDiscussions, CountComments, CountDrafts, CountBookmarks, DateAllViewed,
%%		   Liked, DisLiked, ILiked]).
%% -module(profile, [Id,OwnerId,Userimage,Avatar,AboutMe,
%%                 Strength1,Strength2,Strength3,Strength4,Strength5,
%%                 PerPage,JobTitle,Locale,Signature,CreatedOn,UpdatedOn]).
create_users() ->
    UserGroup = boss_db:find_first(o_group, [{name, "user"}]),
    lists:map(fun(X) ->
		      case boss_db:find_first(owner, [{userid, X:userid()}]) of
			  undefined ->
			      lager:info("Creating user ~p ...~n", [X:name()]),
			      case ldap:ldapinfo(X:email()) of
				  ok ->
				      lager:info("User for email ~p not found in LDAP~n", [X:email()]);
				  {eldap_entry, _ShortInfo, LdapInfo} ->
				      [Firstname] = proplists:get_value("givenName", LdapInfo),
				      [Lastname] = proplists:get_value("sn", LdapInfo),
				      [LdapUid] = proplists:get_value("uidNumber", LdapInfo),
				      Now = erlang:now(),
			      
				      case (owner:new(id, Firstname, Lastname, X:email(), LdapUid, [read], UserGroup:id(), true, X:userid(), Now, Now )):save() of
					  {ok, SavedUser} ->
					      Strength1 = strength(proplists:get_value("strength1", LdapInfo, ["No Strength 1"])),
					      Strength2 = strength(proplists:get_value("strength2", LdapInfo, ["No Strength 2"])),
					      Strength3 = strength(proplists:get_value("strength3", LdapInfo, ["No Strength 3"])),
					      Strength4 = strength(proplists:get_value("strength4", LdapInfo, ["No Strength 4"])),
					      Strength5 = strength(proplists:get_value("strength5", LdapInfo, ["No Strength 5"])),
					      [RawPhoto] = proplists:get_value("photo", LdapInfo, ["No Photo"]),
					      Photo = save_photo(SavedUser, RawPhoto),
					      [JobTitle] = case proplists:get_value("title", LdapInfo, ["No Job Title"]) of
							       [Title] ->
								   lager:info("[Title]: ~p~n",[Title]),
								   [Title];
							       Otherwise ->
								   lager:info("Otherwise: ~p~n", [Otherwise]),
								   [Otherwise]
							   end,
					      Locale = "en",
				      
					      (profile:new(id, SavedUser:id(), Photo, [], [],
							   Strength1, Strength2, Strength3, Strength4, Strength5,
							   10, JobTitle, Locale, [], Now, Now)):save();
					  {error, Error} ->
					      lager:error("ERROR: ~p~n", [Error])
				      end;
				  {error, Reason} ->
				      Reason
			      end;
			  Owner ->
			      %% User already exists, skipping it to avoid duplication
			      lager:info("User ~p already exists", [Owner:fullname()])
		      end
	      end, boss_db:find(gdn_user, [], [{order_by, name}])).

%% ----------------------------------------------------------------------------
%% @doc strength: Strength :: string(),
%% Strength = ["No Strength N" | "R10 - Relator"] 
%% strength( Strength :: string() ) -> Result :: string(), Result = ["No Strength N" | "Relator"]
%% ----------------------------------------------------------------------------
strength(Strength) ->
    case string:str("No Strength",hd(Strength)) of
	0 ->
	    case string:str(hd(Strength), " - ") of
		0 ->
		    hd(Strength);
		_ ->
		    case string:tokens(hd(Strength), " - ") of
			[_, S1] ->
			    %% The String was just the Ranking and the name of the Strength.
			    %% Example: "R10 - Relator".
			    S1;
			[_, P1, P2] ->
			    %% This one is evil, the Strength had a hyphen, thus it's
			    %% split into three (hopefully never more), so we need to
			    %% reconnect the 2nd and 3rd part of the Strength with a new
			    %% hypen to keep the correct Strength name.
			    P1 ++ "-" ++ P2
		    end
	    end;
	Set ->
	    hd(Strength)
    end.

%% A Vanilla "GDN_Category" becomes a "Forum"
%% forum, [Id, ChannelId, OwnerId, Name, Comments, Tags, Oldid, CreatedOn, UpdatedOn]
%% -module(gdn_category, [Id, Categoryid, ParentCategoryid, TreeLeft, TreeRight, Depth, CountDiscussions, CountComments, DateMarkedRead, AllowDiscussions, Archived, Name,
%%		       UrlCode, Description, Sort,  PermissionCategoryid,  InsertUserid,  UpdateUserid,  DateInserted,  DateUpdated,  LastCommentid,  LastDiscussionid,
%%		       Heading,  LockCategory]).
create_forums() ->
    Channel = boss_db:find_first(channel, [{name, "Vanilla Import"}]),
    Admin = boss_db:find_first(owner, [{firstname, "Admin"}]),
    %% Owner = Channel:owner(),
    lists:map(fun(X) ->
		      case boss_db:find(forum, [{name, X:name()},{comments, X:description()},{channelid, Channel:id()}]) of
			  [] ->
			      case boss_db:find_first(owner, [{userid, X:insert_userid()}]) of
				  undefined ->
				      error_logger:error_msg("The owner is no longer in the database, replaced it with admin as the new owner", []),
				      (forum:new(id, Channel:id(), Admin:id(), X:name(), X:description(), [], X:categoryid(), X:date_inserted(), X:date_updated())):save();
				  Owner ->
				      (forum:new(id, Channel:id(), Owner:id(), X:name(), X:description(), [], X:categoryid(), X:date_inserted(), X:date_updated())):save()
			      end;
			  Data ->
			      error_logger:info_msg("Forum with that description already exists. (~p)~n", [X:name()])
		      end
	      end, boss_db:find(gdn_category, [], [{order_by, name}, {descending, false}])).

%% A Vanilla "Discussion" becomes a "Thread"
%% -module(gdn_discussion,[Id,Discussionid,Type,Foreignid,Categoryid,InsertUserid,UpdateUserid,LastCommentid,Name,Body,Format,Tags,CountComments,CountBookmarks,CountViews,
%%                            Closed, Announce,Sink,DateInserted,DateUpdated,InsertIpAddress,UpdateIpAddress,DateLastComment,LastCommentUserid,Score,Attributes,Regardingid,
%%                            State,QnA,DateAccepted,DateOfAnswer]).
%% -module(thread, [Id, ForumId, OwnerId, Name, Comments, Tags, Open, Votes, Oldid, CreatedOn, UpdatedOn]).
create_threads() ->
    Admin = boss_db:find_first(owner, [{firstname, "Admin"}]),
    lists:map(fun(X) ->
		      %% Finding the correct Forum where the discussion should be part of
		      Forum = boss_db:find_first(forum, [{oldid, X:categoryid()}]),
		      Owner = case boss_db:find_first(owner, [{userid, X:insert_userid()}]) of
				  undefined ->
				      Admin;
				  Data ->
				      Data
			      end,
		      %% The tying the right things together ... 
		      (thread:new(id, Forum:id(), Owner:id(), X:name(), X:body(), X:tags(), true, 0, X:discussionid(), X:date_inserted(), X:date_updated())):save()
	      end, boss_db:find(gdn_discussion, [], [{order_by, name}, {descending, false}])).

%% A Vanilla "Comment" becomes a "Topic"
%% topic, [Id, ThreadId, OwnerId, Name, Tags, Body, Solved, SolvedBy, SolvedOn, Votes, CreatedOn, UpdatedOn]).
%% -module(gdn_comment, [Id, AcceptedUserid, Attributes, Body, Commentid,
%%		      DateAccepted, DateDeleted, DateInserted, DateUpdated,
%%		      Deleteuserid, Discussionid, Flag, Format,
%%		      InsertIpAddress, Insertuserid, Qna, Score, UpdateIpAddress, Updateuserid]).
%% create_topics() ->
%%     lists:map(fun(X) ->
%% 		      Thread = boss_db:find_first(thread, [{oldid, X:discussionid()}]),
%% 		      Owner = boss_db:find_first(owner, [{userid, X:insertuserid()}]),
%% 		      AcceptedUser = boss_db:find(owner, [{userid, X:accepted_userid()}]),

%% 		      topic:new(id, Thread:id(), Owner:id(), [], [], X:body(), [], AcceptedUser:id(), X:date_accepted(), 0, X:date_inserted(), X:date_updated())
%% 	      end, boss_db:find(gdn_comment, [], [{order_by, name}, {descending, false}])).
create_topics() ->
    lists:map(fun(X) ->
		      case boss_db:find_first(thread, [{oldid, X:discussionid()}]) of
			  undefined ->
			      error_logger:error_msg("Thread not found: ~d~n", [X:commentid()]);
			  Thread ->
			      Owner = finduser_safely({userid, X:insertuserid()}),
			      AcceptedUser = finduser_safely({userid, X:accepted_userid()}),
			      (topic:new(id, Thread:id(), Owner:id(), [], [], X:body(), [], AcceptedUser:id(), X:date_accepted(), 0, X:date_inserted(), X:date_updated())):save()
		      end
	      end, boss_db:find(gdn_comment, [])).

finduser_safely({Key, Value}) ->
    case boss_db:find_first(owner, [{Key, Value}]) of
	undefined ->
	    boss_db:find_first(owner, [{firstname, "Admin"}, {lastname, "Account"}]);
	FoundOwner ->
	    FoundOwner
    end.

fix_tags([], _) ->
    io:format("Empty  Tags~n");
fix_tags(X, Rec) when is_binary(X) ->
    io:format("Binary Tags: ~p~n", [binary_to_list(X)]),
    Temp = binary_to_list(X),
    Split = string:tokens(Temp, " "),
    NewTags = string:join(Split, ","),
    (Rec:set([{tags, NewTags}])):save();
fix_tags(X, Rec) when is_list(X) ->
    io:format("List   Tags: ~p~n", [X]),
    Split = string:tokens(X, " "),
    NewTags = string:join(Split, ","),
    (Rec:set([{tags, NewTags}])):save().

fix_thread_tags() ->
    lists:map(fun(X) -> 
		      fix_tags(X:tags(), X)
	      end, boss_db:find(thread, [])).

fix_topic_tags() ->
    lists:map(fun(X) ->
		      fix_tags(X:tags(), X)
	      end, boss_db:find(topic, [])).
