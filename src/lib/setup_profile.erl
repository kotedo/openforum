-module(setup_profile).
-export([create_or_update/1]).

-include("../../include/permissions.hrl").

create_or_update(LDAPInfo) ->
    %% owner,  [Id, Firstname, Lastname, Email, LdapUid, Userlevel,  Groups, Active, CreatedOn, UpdatedOn]
    %% owner,  [Id, Firstname, Lastname, Email, LdapUid, Permissions,Groups, Active, Userid,CreatedOn,UpdatedOn]).
    %% profile,[Id, OwnerId, Userimage, Avatar, AboutMe, Strength1, Strength2, Strength3, Strength4, Strength5, CreatedOn, UpdatedOn]
    [LDAPUid] = proplists:get_value(uid, LDAPInfo),
    %% error_logger:info_msg("Fishing for a user with LDAP id ~p~n", [LDAPUid]),

    Owner = boss_db:find_first(owner, [{ldap_uid, LDAPUid}]),
    case Owner of
		undefined ->
		    %% Create record data from what we found out in LDAP
		    error_logger:info_msg("Owner (undefined): ~p~n"
					  "LDAP             : ~p~n", [Owner, LDAPInfo]),
		    Firstname = list_to_binary(proplists:get_value(first, LDAPInfo)),
		    Lastname = list_to_binary(proplists:get_value(last, LDAPInfo)),
		    Email = list_to_binary(proplists:get_value(email, LDAPInfo)),
		    LdapUid = list_to_binary(proplists:get_value(uid, LDAPInfo)),

		    %% Now set up defaults
		    Userlevel = ?USER,
		    %% Set the initial group to a regular "user"
		    Groups = (boss_db:find_first(o_group, [{name, "user"}])):id(),
		    
		    %% Setup the record
		    TempOwner = owner:new(id, Firstname, Lastname, Email, LdapUid, Userlevel, Groups, true, 0, erlang:now(), []),
		    %% Save the record and return its outcome
		    error_logger:info_msg("TempOwner: ~p~n", [TempOwner]),
		    case TempOwner:save() of
			{ok, SavedOwner} ->
			    SavedOwner;
			{error, Reason} ->
			    error_logger:error_msg("REASON: ~p~n", [Reason]),
			    Reason
		    end;
	        FoundOwner ->
		    error_logger:info_msg("FoundOwner: ~p~n", [FoundOwner]),
		    FoundOwner
	    end,
 
    case  Owner:profile() of
	undefined ->
	    error_logger:info_msg("Profile for Owner id ~p not found, we create a profile now.~n", [Owner:fullname()]),
	    %% Setting up a few dummy Profile variables
	    AboutMe = "",

	    %% Setup the initial userimage, "Avatar" is customizable and if it is set, it overrides "Userimage" in the UI
	    Userimage = lists:flatten(string:concat( "/static/userimages/", [proplists:get_value( username, LDAPInfo), ".jpg"])),

	    %% No personalized avatar just yet
	    Avatar = "",

	    %% Setup the 5 strengths
	    Strengths = proplists:get_value(strengths, LDAPInfo),
	    Strength1 = get_strength(strength1, Strengths),
	    Strength2 = get_strength(strength2, Strengths),
	    Strength3 = get_strength(strength3, Strengths),
	    Strength4 = get_strength(strength4, Strengths),
	    Strength5 = get_strength(strength5, Strengths),

	    %% Setup the Profile and link it to the owner and save it.
	    PerPage = [],
	    %% Setting the JobTitle
	    JobTitle = proplists:get_value(title, LDAPInfo),
	    %% Presetting the Locale to en-us
	    Locale = list_to_binary(proplists:get_value(locale, LDAPInfo, "en-us")),
	    %% Signature added as of PT ID #47819975
	    Signature = [],
	    %% Creating the "object" (ouch)
	    Profile = profile:new(id, Owner:id(), Userimage, Avatar, AboutMe, Strength1, Strength2, Strength3, Strength4, Strength5, PerPage, JobTitle, Locale, Signature, erlang:now(), []),
	    %% ... and persisting it.
	    Profile:save();
	Data ->
	    %% Already exists
	    ok
    end.

strength_list(LDAPInfo) ->
    Strengths = proplists:get_value(strengths, LDAPInfo),
    [S1] = proplists:get_value( strength1, Strengths),
    [S2] = proplists:get_value( strength2, Strengths),
    [S3] = proplists:get_value( strength3, Strengths),
    [S4] = proplists:get_value( strength4, Strengths),
    [S5] = proplists:get_value( strength5, Strengths),
    
    [_, Strength1] = string:tokens(S1, "- "),
    [_, Strength2] = string:tokens(S2, "- "),
    [_, Strength3] = string:tokens(S3, "- "),
    [_, Strength4] = string:tokens(S4, "- "),
    [_, Strength5] = string:tokens(S5, "- ").
    
%% Strength1 = get_strength( strength1, Strengths ) 
get_strength(Which, Strengths) ->
    Temp = lists:flatten(proplists:get_value( Which, Strengths, "no_strength_set")),
    error_logger:info_msg("Temp is ~p~n", [Temp]),
    Strength = case string:tokens(Temp, "- ") of
		   [_, Data] ->
		       Data;
		   [Else] ->
		       Else
	       end,
    Strength.
