-module(import).
-compile(export_all).

%% vowner, [Id, Userid, Name,  Password,  HashMethod,  Photo,  About,  Email,  ShowEmail,  Gender,  CountVisits,  CountInvitations,  CountNotifications,  InviteUserid,
%%  DiscoveryText,  Preferences,  Permissions,  Attributes,  DateSetInvitations,  DateOfBirth,  DateFirstVisit,  DateLastActive,  LastIpAddress,  DateInserted,
%%  InsertIpAddress,  DateUpdated,  UpdateIpAddress,  HourOffset,  Score,  Admin,  Banned,  Deleted,  CountUnreadConversations,  CountDiscussions,  CountUnreadDiscussions,
%%  CountComments,  CountDrafts,  CountBookmarks,  DateAllViewed,  Liked,  DisLiked,  ILiked]
%% owner, [Id, Firstname, Lastname, Email, LdapUid, Permissions, Groups, Active, CreatedOn, UpdatedOn]

%% Mapping:
%% Id -> Id
%% UserId -> ?
%% Name -> Firstname, Lastname
%% Email -> Email
%% ? -> LdapUid
%% Admin -> Permissions
%% Deleted -> Active
%% DateInserted -> CreatedOn
%% DateUpdated -> UpdatedOn
-include("../../include/permissions.hrl").

%% User Creation Date / Update Date
dates(undefined) ->
    {{Year,Month,Day},{H,M,S}} = calendar:universal_time_to_local_time( calendar:now_to_universal_time(erlang:now())),
    lists:flatten(io_lib:format("~2.10.0B/~2.10.0B/~4.10.0B ~2.10.0B:~2.10.0B:~2.10.0B", [Month, Day, Year, H, M, S]));
dates(X) ->
    {{Year,Month,Day},{H,M,S}} = calendar:universal_time_to_local_time( calendar:now_to_universal_time(X)),
    lists:flatten(io_lib:format("~2.10.0B/~2.10.0B/~4.10.0B ~2.10.0B:~2.10.0B:~2.10.0B", [Month, Day, Year, H, M, S])).

%% Gets the Lastname of a User; can be tricky!
getlast([], _Len) ->
    [];
getlast(Name, Len) when Name =/= [] ->
    %% io:format("getlast: NAME: ~p~n", [Name]),
    string:substr(Name, Len + 2).

%% PASS1: Importing Users
pass1() ->
    %% Read through the database and dump out the mapping
    RawData = boss_db:find(vowner, [], [{order_by, name}]),
    %% Let's print it
    lists:map( fun(X) ->
		       Name = binary_to_list(X:name()),
		       First = string:sub_word(Name, 1),
		       Len = string:len(First),
		       Last = getlast(Name, Len),
		       Email = binary_to_list(X:email()),
		       CreatedOn = dates(X:date_inserted()),
		       UpdatedOn = dates(X:date_updated()),
		       io:format("First: ~p\tLast: ~p\t~p\t~p\t~p~n",[
								  First, Last,
								  X:userid(),
								  Email,
								  dates(X:date_inserted())
								 ]),
		       case boss_db:find_first(owner, [{userid, X:userid()}]) of
			   undefined ->
			       io:format("User ~p is not in the database yet.~n", [X:name()]),
			       (owner:new(id, First, Last, Email, [], ?USER, <<"o_group-512be2d469e5817b5d000002">>, true, X:userid(), CreatedOn, UpdatedOn)):save();
			   Else ->
			       io:format("~n~nUser ~p is already in the database.~n~~n", [Else])
		       end

	       end, RawData ).

%% PASS2: Correcting users' LdapUid
pass2() ->
    %% boss_db:find(owner, [{ldap_uid, []}]),
    Count = boss_db:count(owner, [{ldap_uid,'not_matches', [] }]),
    Data = boss_db:find(owner, [{ldap_uid,'not_matches', [] }]),

    io:format("~n~n~n=============================== Right before anon func(X) -> ==================================== ~n~n~n"),
    %% [fun(Y) -> 
    %% 	      LdapUid = ldap:search_user(Y:email()),
    %% 	      io:format("LdapUid for ~p is ~p~n", [binary_to_list(Y:email()), LdapUid ])
    %%   end || Y <- Data],

    lists:map(fun(X) -> LdapUid = ldap:search_user(X:email()),
			io:format("LdapUid for ~p is ~p (Vanilla Userid: ~p)~n", [binary_to_list(X:email()), LdapUid, X:userid()]),
			Old = boss_db:find_first(owner, [{userid, X:userid()}]),
			(Old:set([{ldap_uid, LdapUid}])):save()
	      end, Data),

    io:format("~n~n~n=============================== Right after  anon func(X) -> ==================================== ~n~n~nData length: ~p~n", [length(Data)]),
    {ok, Data, Count}.

%% Migrating the Hierachially sorted categories into a flat structure
pass3_old() ->
    lists:map(fun(X) ->
		      %%io:format("Name: ~p~nDescription: ~p~n",
		%%		[
		%%		 binary_to_list(X:name()),
		%%		 binary_to_list(X:description())
		%%		]),
		      %% 
		      %% channel:new(id, )
		      %% [Id, Name, OwnerId, Description, Position, Tags, CreatedOn, UpdatedOn]).
		      Name = binary_to_list(X:name()),
		      OwnerId = (boss_db:find_first(owner, [{userid, X:insert_userid()}])):id(),
		      %% OwnerId = [],
		      Description = binary_to_list(X:description()),
		      Position = 0,
		      Tags = binary_to_list(X:url_code()),
		      %%CreatedOn = dates:to_now(X:date_inserted()),
		      %%UpdatedOn = dates:to_now(X:date_updated()),
		      CreatedOn = X:date_inserted(),
		      UpdatedOn = X:date_updated(),
		      Userid = X:insert_userid(),
		      Oldid = X:categoryid(),
		      io:format("RECORD: ~p~n", [channel:new(id, Name, OwnerId, Description, Position, Tags, Userid, Oldid, CreatedOn, UpdatedOn)]),
		      case X:depth() =:= 1 of
			  true ->
			      %% (channel:new(id, Name, OwnerId, Description, Position, Tags, Userid, Oldid, CreatedOn, UpdatedOn)):save();
			      ok;
			  false ->
			      %% NOT a Channel, but a Forum

			      case X:depth() of
				  2 ->
				      Parentid = X:parent_categoryid(),
				      ParentRec = boss_db:find(vcategory, [{categoryid, Parentid}]),
				      %% [Id, ChannelId, OwnerId, Name, Comments, Tags, CreatedOn, UpdatedOn]).
				      
				      %% forum:new(id, ParentRec:id(), )
				      ok;
				  3 ->
				      ok;
				  4 ->
				      ok;
				  5 ->
				      ok;
				  6 ->
				      ok;
				  Else ->
				      boom
			      end
		      end
	      end, boss_db:find(vcategory, [])).

%% pass3() ->
%%     Channels = boss_db:find(channel, []),
%%     Oldid belongs to vcategory:categoryid
%%     Match all vcategory that have vcategory:categoryid =:= oldid
%%     Attach to channel, channel:id() -> 

%%     forum, [Id, ChannelId, OwnerId, Name, Comments, Tags, CreatedOn, UpdatedOn]).
    
%%     lists:map( fun(X) -> 
%% 		       OldVcategory = boss_db:find(vcategory, [{categoryid, X:oldid()}]),
%% 		       OwnerId = (boss_db:find_first(owner, [{userid, OldVcategory:insert_userid()}])):id()
%% 		       (forum:new(id, X:id(), Ownerid, Name, Comments, Tags, CreatedOn, UpdatedOn)):save(),
%% 		       Channels
%% 	       end
%%      )
%%     ok.


%% lists:map( fun(Old) ->
%% 		   (forum:new(id, "channel-5159e31569e581c9ab000003",
%% 			      (boss_db:find_first(owner, [{userid, Old:insert_userid()}])):id(),
%% 			      binary_to_list(Old:name()),
%% 			      binary_to_list(Old:description()),
%% 			      binary_to_list(Old:url_code()),
%% 			      Old:date_inserted(),
%% 			      Old:date_updated())):save()
%% 	   end, boss_db:find(vcategory, [{parent_categoryid, 28 }])).


%% MISC channel-515ca7d569e581c2b500000d

create_misc(ChannelId) ->
    lists:map( fun(Category) ->
		       lists:map( fun(Old) ->
					  (forum:new(id, ChannelId,
						     (boss_db:find_first(owner, [{userid, Old:insert_userid()}])):id(),
						     binary_to_list(Old:name()),
						     binary_to_list(Old:description()),
						     binary_to_list(Old:url_code()),
						     Oldid = Category,
						     Old:date_inserted(),
						     Old:date_updated())):save()
				  end, boss_db:find(vcategory, [{categoryid, Category }]))
	       end,
	       [69, 46, 66, 45, 21, 68, 40, 30, 39, 38]
	       ).



%% thread [Id, ForumId, OwnerId, Name, Comments, Tags, Open, Votes, CreatedOn, UpdatedOn]).

%% lists:map( fun(X) -> (thread:new(id, "forum-515c8d6069e58198b000000f",
%% 				 "owner-512be2d469e5817b5d000003",
%% 				 binary_to_list(X:name()),
%% 				 binary_to_list(X:body()),
%% 				 ["not_tagged_yet"],
%% 				 X:date_inserted(),
%% 				 X:date_updated())):save()
%% 	   end,
%% 	   boss_db:find(vdiscussion,
%% 			[{categoryid, 45}],
%% 			[
%% 			 {order_by, date_last_comment},
%% 			 descending
%% 			]))

%% thread, [Id, ForumId, OwnerId, Name, Comments, Tags, Open, Votes, CreatedOn, UpdatedOn])

create_boldchat() ->
    lists:map( fun(Old) ->
		       (thread:new(id,
				   "forum-515c8d6069e58198b000000f",
				   (boss_db:find_first(owner, [{userid, Old:insert_userid()}])):id(),
				   binary_to_list(Old:name()),
				   binary_to_list(Old:body()),
				   ["not_tagged_yet"],
				   true,
				   0,
				   Old:date_inserted(),
				   Old:date_updated())):save()
	       end,
	       
	       boss_db:find(vdiscussion,
			    [{categoryid, 45}],
			    [
			     {order_by, date_last_comment},
			     descending
			    ]
			   )),
    ok.

%% topic,  [Id, ThreadId, OwnerId, Name, Tags, Body, Solved, SolvedBy, SolvedOn, Votes, CreatedOn, UpdatedOn]).
%% (vcomment, [Id,Commentid, Discussionid,InsertUserid,UpdateUserid,DeleteUserid,Body,Format,DateInserted,
%%             DateDeleted,DateUpdated,InsertIpAddress,UpdateIpAddress,Flag,Score,Attributes,Qna,DateAccepted,AcceptedUserid])
%% thread, [Id, ForumId, OwnerId, Name, Comments, Tags, Open, Votes, Oldid, CreatedOn, UpdatedOn])


make_misc() ->
    lists:map( fun(T) ->
		      create_threads(T)
	      end,
	      [69, 46, 66, 45, 21, 68, 40, 30, 39, 38]
	    ).

create_threads(CategoryId) ->
    lists:map( fun(Old) ->
		       (thread:new(id, (boss_db:find_first(forum, [{oldid, CategoryId}])):id(),
				 (boss_db:find_first(owner, [{userid, Old:insert_userid()}])):id(),
				 binary_to_list(Old:name()),
				 binary_to_list(Old:body()),
				 binary_to_list(Old:tags()),
				 true,
				 0,
				 Old:discussionid(),
				 Old:date_inserted(),
				 Old:date_updated()
				)):save()
	       end,
	       boss_db:find(vdiscussion, [{categoryid, CategoryId}])).

%% thread-515cbf8569e581e13f00000c
add_topics(ThreadId) ->
    OldId = (boss_db:find(ThreadId)):oldid(),
    VCommentList = boss_db:find(vcomment, [{discussionid, OldId}]),

    lists:map( fun(Old) ->
		       %% topic,  [Id, ThreadId, OwnerId, Name, Tags, Body, Solved, SolvedBy, SolvedOn, Votes, CreatedOn, UpdatedOn]).
		       (topic:new(id, ThreadId,
				 (boss_db:find_first(owner, [{userid, Old:insert_userid()}])):id(),
				 [],
				 [],
				 binary_to_list(Old:body()),
				 false,
				 [],
				 [],
				 0,
				 Old:date_inserted(),
				 Old:date_updated()				 
				)):save()
	       end,
	       VCommentList).



	       

%% do_it_all() ->
%%     %% Find all discussions
%%     TargetList = boss_db:find(thread, []),
%%     %% Find the correct thread id to attach the topics to
%%     lists:map( fun(VTopic) ->
%% 		       create_topics(TargetList:id(), VTopic:discussionid())
%% 	       end,
	       


%%     lists:map( fun(Target) ->
%% 		       Target:disc
%% 	       end,
%% 	       boss_db:find(vcomment,
%% 			    [{discussionid, Discussions:).

