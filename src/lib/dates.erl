-module(dates).
-export([to_now/1]).

% Number of seconds between beginning of gregorian calendar and 1970                                                                                                              
% Thank you Evan Miller
-define(GREGORIAN_SECONDS_1970, 62167219200).

to_now(DateTime) ->
    [Date, Time] = string:tokens(DateTime, " "),
    Tuple = date_from_string(Date, Time),
    datetime_to_now(Tuple).

%% Input format is American Month/Day/Year
%% or
%%                          Month/Year
%% anything else is invalid
date_from_string(Date, Time) when is_list(Date), is_list(Time) ->
    case string:tokens(Date, "/") of
	[Month, Day, Year] ->
	    [Hours, Minutes, Seconds] = string:tokens(Time, ":"),
	    {{list_to_integer(Year), list_to_integer(Month), list_to_integer(Day)}, {list_to_integer(Hours),list_to_integer(Minutes),list_to_integer(Seconds)}};
	[Month, Year] ->
	    {{list_to_integer(Year), list_to_integer(Month), 1},{0,0,0}};
	_ -> undefined
    end.

%% Create a erlang:now() timestamp from a datatime tuple
%% {{Year, Month, Day},{Hour, Minute, Seconds}}
%% i.e.
%% {{2012,5,3},{22,31,59}}
datetime_to_now(DateTime) ->
    GSeconds = calendar:datetime_to_gregorian_seconds(DateTime),
    ESeconds = GSeconds - ?GREGORIAN_SECONDS_1970,
    {ESeconds div 1000000, ESeconds rem 1000000, 0}.
