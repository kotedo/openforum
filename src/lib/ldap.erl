-module(ldap).
-export([login/3,
         logout/1,
         search_user/1,
         ldapinfo/1,
         write_userimage/2]).
-include_lib("eldap/include/eldap.hrl").

search_user(Email) when is_binary(Email) ->
    search_user(binary_to_list(Email));
search_user(Email) ->
    search1(Email).

ldapinfo(Email) ->
    search_all(Email).

search1(Email) ->
    check_required_apps(),
    {ok, Hostname} = application:get_env(openforum, rack_ldap_host),
    {ok, SSL} = application:get_env(openforum, rack_ldap_ssl),
    {ok, Port} = application:get_env(openforum, rack_ldap_port),
    %% {ok, Debug} = application:get_env(openforum, debug),
    try
        [UidNumber] = case eldap:open(Hostname, [{ssl, SSL}, {port, Port}]) of
                          {error, Reason} ->
                              error_logger:error_msg("Could not start LDAP due to this error: ~p~n", [Reason]),
                              {error, Reason};
                          {ok, LDAPPid} ->
                              LDAPInfo = emailsearch(LDAPPid, Email),
                              eldap:close(LDAPPid),
                              get("uidNumber", LDAPInfo)
                      end,
        list_to_binary(UidNumber)
    catch
        error:badarg ->
            error_logger:error_msg("Email ~p could not be found in LDAP.~n", [Email]);
        Error:Class ->
            error_logger:error_msg("Error: ~p~n"
                                   "Class: ~p~n",[Error, Class])
    end.

search_all(Email) ->
    check_required_apps(),
    {ok, Hostname} = application:get_env(openforum, rack_ldap_host),
    {ok, SSL} = application:get_env(openforum, rack_ldap_ssl),
    {ok, Port} = application:get_env(openforum, rack_ldap_port),
    %% {ok, Debug} = application:get_env(openforum, debug),
    try
        case eldap:open(Hostname, [{ssl, SSL}, {port, Port}]) of
            {error, Reason} ->
                error_logger:error_msg("Could not start LDAP due to this error: ~p~n", [Reason]),
                {error, Reason};
            {ok, LDAPPid} ->
                LDAPInfo = emailsearch(LDAPPid, Email),
                eldap:close(LDAPPid),
                LDAPInfo
        end
    catch
        error:badarg ->
            error_logger:error_msg("Email ~p could not be found in LDAP.~n", [Email]);
        Error:Class ->
            error_logger:error_msg("Error: ~p~n"
                                   "Class: ~p~n",[Error, Class])
    end.

logout(SessionID) ->
    boss_session:delete_session(SessionID).

login(SessionID, SSO, Password) ->
    check_required_apps(),
    Username = string:to_lower(SSO),
    LdapSearchString = lists:flatten(io_lib:format("cn=~s, ou=users, o=rackspace", [Username])),

    {ok, Hostname} = application:get_env(openforum, rack_ldap_host),
    {ok, SSL} = application:get_env(openforum, rack_ldap_ssl),
    {ok, Port} = application:get_env(openforum, rack_ldap_port),
    {ok, Debug} = application:get_env(openforum, debug),
    try
        case eldap:open(Hostname, [{ssl, SSL}, {port, Port}]) of
            {error, Reason} ->
                {error, Reason};
            {ok, LDAPPid} ->
                case Debug of
                    true ->
                        error_logger:info_msg("Before eldap:simple_bind()~n~n"
                                              "Username : ~p~n"
                                              "SessionID: ~p~n"
                                              "Hostname : ~p~n"
                                              "SSL      : ~p~n"
                                              "Port     : ~p~n"
                                              "Pid      : ~p~n", [Username, SessionID, Hostname, SSL, Port, LDAPPid]);
                    _ ->
                        ok
                end,
                case eldap:simple_bind(LDAPPid, LdapSearchString, Password) of
                    ok ->
                        LDAPInfo = mysearch(LDAPPid, Username),
                        eldap:close(LDAPPid),
                        First = get("givenName", LDAPInfo),
                        Last = get("sn", LDAPInfo),
                        Fullname = get("displayName", LDAPInfo),
                        Photo = get("photo", LDAPInfo),
                        save_photo(Photo, Username),
                        Email = get("mail", LDAPInfo),
                        UidNumber = get("uidNumber", LDAPInfo),
                        Title = get("title", LDAPInfo),
                        %% Setting the userlevel
                        setuserlevel(Username),
                        Strengths = [{strength1,get("strength1",LDAPInfo)},
                                     {strength2,get("strength2",LDAPInfo)},
                                     {strength3,get("strength3",LDAPInfo)},
                                     {strength4,get("strength4",LDAPInfo)},
                                     {strength5,get("strength5",LDAPInfo)}
                                    ],

                        MyLdapInfo = [{first, First}, {last, Last}, {fullname, Fullname},
                                      {email, Email}, {username, Username}, {uid, UidNumber},
                                      {strengths, Strengths}, {title, Title}, {locale, "en"}],

                        setenvironment(SessionID, [{fullname, Fullname}, {username, Username},
                                                   {email,Email}, {locale, "en"}, {ldap_uid, UidNumber},
                                                   {title, Title}, {strength, Strengths}]),

                        %% Let's see if we have profile information for this user
                        case boss_db:find_first(owner, [{ldap_uid, UidNumber}]) of
                            undefined ->
                                error_logger:info_msg("(LDAP): No profile found for user ~s, setting up profile now.~nData:~p~n", [Fullname, MyLdapInfo]),
                                setup_profile:create_or_update(MyLdapInfo);
                            Data ->
                                %% No see if we have profile data (we SHOULD!!!)
                                Profile = case Data:profile() of
                                              undefined ->
                                                  %% No profile yet, let's make one
                                                  setup_profile:create_or_update(MyLdapInfo),
                                                  %% Now return it
                                                  Data:profile();
                                              ProfileInfo ->
                                                  %% Life is good, it's already in place, just return it
                                                  %%error_logger:info_msg("Data: ~p~n", [Data]),
                                                  ProfileInfo
                                          end,
                                boss_session:set_session_data(SessionID, locale, binary_to_list(Profile:locale())),
                                %% In case we enabled debugging in the settings, dump some debugging info
                                case Debug of true -> error_logger:info_msg("Data: ~p~n", [Data]); _ -> ok end
                        end,
                        %% Returning MyLdapInfo, which is NOT the same as LDAPInfo returned from ldap:search()
                        {ok, MyLdapInfo};
                    {error, OtherLDAPError} ->
                        error_logger:error_msg("(LDAP): Something else went terribly wrong: ~p~n", [OtherLDAPError]),
                        {error, OtherLDAPError}
                end
        end
    catch
        Error:Class ->
            error_logger:error_msg("Error: ~p~n"
                                   "Class: ~p~n",[Error, Class])
    end.

check_required_apps() ->
    case proplists:is_defined(public_key, application:which_applications()) of
        false ->
            application:start(public_key);
        true ->
            ok
    end,

    case proplists:is_defined(ssl, application:which_applications()) of
        false ->
            application:start(ssl);
        true ->
            ok
    end.

mysearch(Pid, Username) ->
    Base = {base, pick(application:get_env(openforum, rack_ldap_base))},
    Scope = {scope, pick(eldap:wholeSubtree())},
    Filter = {filter, eldap:equalityMatch("uid", Username)},
    Search = [Base, Scope, Filter],
    {ok, Result} = eldap:search(Pid, Search),
    hd(Result#eldap_search_result.entries).

emailsearch(Pid, Email) ->
    Base = {base, pick(application:get_env(openforum, rack_ldap_base))},
    Scope = {scope, pick(eldap:wholeSubtree())},
    Filter = {filter, eldap:equalityMatch("mail", Email)},
    Search = [Base, Scope, Filter],
    {ok, Result} = eldap:search(Pid, Search),
    hd(Result#eldap_search_result.entries).

get(What, Data) ->
    proplists:get_value(What, Data#eldap_entry.attributes).

save_photo([], []) ->
    error_logger:error_msg("Photo Data is empty and no Username");
save_photo(_Photo, []) ->
    error_logger:error_msg("I've got Photo data, but no Username");
save_photo([], _Username) ->
    error_logger:error_msg("I've got no Photo data but a Username");
save_photo(Photo, Username) ->
    Filename = lists:flatten(io_lib:format("priv/static/userimages/~s.jpg", [Username])),
    spawn(ldap, write_userimage, [Filename, Photo]).

write_userimage(Filename, Photo) ->
    error_logger:info_msg("Saving file ~p in extra process~n", [Filename]),
    {ok, File} = file:open(Filename, [write]),
    file:write(File, Photo),
    file:close(File),
    error_logger:info_msg("Done writing file ~p~n", [Filename]).

pick([]) ->
    [];
pick({_ReturnValue, Value}) ->
    Value;
pick(Anything) ->
    Anything.

setuserlevel(_Username) ->
    ok.

setenvironment(SessionID, List) ->
    [ boss_session:set_session_data(SessionID, Key, Value) || {Key, Value} <- List].
