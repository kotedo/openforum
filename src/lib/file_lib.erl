-module(file_lib).
-compile(export_all).

avatar([{uploaded_file, _Filename, TempAvatar, _Size, _Field}], Username) ->
    save_avatar(TempAvatar, Username).

save_avatar([], []) ->
    error_logger:error_msg("Photo Data is empty and no Username");
save_avatar(_Source, []) ->
    error_logger:error_msg("I've got Photo data, but no Username");
save_avatar([], _Username) ->
    error_logger:error_msg("I've got no Photo data but a Username");
save_avatar(Source, Username) ->
    error_logger:info_msg("~n~nSAVE AVATAR:~nSource: '~s'~nUsername: '~s'~n~n", [Source, Username]),
    Destination = lists:flatten(io_lib:format("static/avatars/~s.jpg", [Username])),
    file:copy(Source, filename:join("priv", Destination)),
    "/" ++ Destination.
