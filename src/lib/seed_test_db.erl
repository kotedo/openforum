-module(seed_db).
-compile(export_all).
-include("../../include/permissions.hrl").

add_admin_group() ->
    (o_group:new(id, "admin")):save(),
    (o_group:new(id, "user")):save().

admin_group() ->
    (boss_db:find_first(o_group, [{name, "admin"}])):id().

add_admin_account() ->
    %% [Id, Firstname, Lastname, Email, LdapUid, Permissions, Groups, Active, CreatedOn, UpdatedOn]
    %% Repeat this schema for more "default" users
    Firstname = "Admin",
    Lastname = "Account",
    Email = "forum_admin@rackspace.com",
    LdapUid = "0",
    Permissions = ?ADMIN,
    Groups = [admin_group()],
    Active = true,
    Userid = 0,
    CreatedOn = erlang:now(),
    UpdatedOn = [],
    (owner:new(id, Firstname, Lastname, Email, LdapUid, Permissions, Groups, Active, Userid, CreatedOn, UpdatedOn )):save().

setup_strengths() ->
    (strength:new(id, "Achiever", "People strong in the Achiever theme have a great deal of stamina and work hard. They take great satisfaction from being busy and productive.", "")):save(),
    (strength:new(id, "Activator", "People strong in the Activator theme can make things happen by turning thoughts into action. They are often impatient.", "")):save(),
    (strength:new(id, "Adaptability", "People strong in the Adaptability theme prefer to \"go with the flow.\" They tend to be \"now\" people who take things as they come and discover the future one day at a time.", "")):save(),
    (strength:new(id, "Analytical", "People strong in the Analytical theme search for reasons and causes. They have the ability to think about all the factors that might affect a situation.", "")):save(),
    (strength:new(id, "Arranger", "People strong in the Arranger theme can organize,but they also have a flexibility that complements this ability. They like to figure out how all of the pieces and resources can be arranged for maximum productivity", "")):save(),
    (strength:new(id, "Belief", "People  strong in the Belief theme have certain core values that are unchanging. Out of these values emerges a defined purpose for their life.", "")):save(),
    (strength:new(id, "Command", "People  strong in the Command theme have presence. They can take control of a situation and make decisions.", "")):save(),
    (strength:new(id, "Communication", "People  strong in the Communication theme generally find it easy to put their thoughts into words. They are good conversationalists and presenters.", "")):save(),
    (strength:new(id, "Competition", "People  strong in the Competition theme measure their progress against the performance of others. They strive to win first place and revel in contests.", "")):save(),
    (strength:new(id, "Connectedness", "People  strong in the Connectedness theme have faith in the links between all things. They believe there are few coincidences and that almost every event has a reason.", "")):save(),
    (strength:new(id, "Consistency", "People strong in the Consistency theme (also called Fairness in the first StrengthsFinder assessment) are keenly aware of the need to treat people the same. They try to treat everyone in the world fairly by setting up clear rules and adhering to them.", "")):save(),
    (strength:new(id, "Context", "People  strong in the Context theme enjoy thinking about the past. They understand the present by researching its history.", "")):save(),
    (strength:new(id, "Deliberative", "People  strong in the Deliberative theme are best described by the serious care they take in making decisions or choices. They anticipate the obstacles.", "")):save(),
    (strength:new(id, "Developer", "People  strong in the Developer theme recognize and cultivate the potential in others. They spot the signs of each small improvement and derive satisfaction from these improvements.", "")):save(),
    (strength:new(id, "Discipline", "People strong in the Discipline theme enjoy routine and structure. Their world is best described by the order they create.", "")):save(),
    (strength:new(id, "Empathy", "People strong in the Empathy theme can sense the feelings of other people by imagining themselves in others' lives or others' situations.", "")):save(),
    (strength:new(id, "Fairness", "People strong in the Consistency theme (also called Fairness in the first StrengthsFinder assessment) are keenly aware of the need to treat people the same. They try to treat everyone in the world fairly by setting up clear rules and adhering to them.", "")):save(),
    (strength:new(id, "Focus", "People strong in the Focus theme can take a direction,follow through,and make the corrections necessary to stay on track. They prioritize,then act.", "")):save(),
    (strength:new(id, "Futuristic", "People  strong in the Futuristic theme are inspired by the future and what could be. They inspire others with their visions of the future.", "")):save(),
    (strength:new(id, "Harmony", "People  strong in the Harmony theme look for consensus. They don't enjoy conflict; rather, they seek areas of agreement.", "")):save(),
    (strength:new(id, "Ideation", "People  strong in the Ideation theme are fascinated by ideas. They are able to find connections between seemingly disparate phenomena.", "")):save(),
    (strength:new(id, "Includer", "People  strong in the Inclusiveness theme are accepting of others. They show awareness of those who feel left out, and make an effort to include them.", "")):save(),
    (strength:new(id, "Inclusiveness", "People  strong in the Inclusiveness theme are accepting of others. They show awareness of those who feel left out, and make an effort to include them.", "")):save(),
    (strength:new(id, "Individualization", "People  strong in the Individualization theme are intrigued with the unique qualities of each person. They have a gift for figuring out how people who are different can work together productively.", "")):save(),
    (strength:new(id, "Input", "People  strong in the Input theme have a craving to know more. Often they like to collect and archive all kinds of information.", "")):save(),
    (strength:new(id, "Intellection", "People  strong in the Intellection theme are characterized by their intellectual activity. They are introspective and appreciate intellectual discussions.", "")):save(),
    (strength:new(id, "Learner", "People  strong in the Learner theme have a great desire to learn and want to continuously improve. In particular, the process of learning, rather than the outcome, excites them.", "")):save(),
    (strength:new(id, "Maximizer", "People  strong in the Maximizer theme focus on strengths as a way to stimulate personal and group excellence. They seek to transform something strong into something superb.", "")):save(),
    (strength:new(id, "Positivity", "People  strong in the Positivity theme have an enthusiasm that is contagious. They are upbeat and can get others excited about what they are going to do.", "")):save(),
    (strength:new(id, "Relator", "People  who are strong in the Relator theme enjoy close relationships with others. They find deep satisfaction in working hard with friends to achieve a goal.", "")):save(),
    (strength:new(id, "Responsibility", "People  strong in the Responsibility theme take psychological ownership of what they say they will do. They are committed to stable values such as honesty and loyalty.", "")):save(),
    (strength:new(id, "Restorative", "People  strong in the Restorative theme are adept at dealing with problems. They are good at figuring out what is wrong and resolving it.", "")):save(),
    (strength:new(id, "Self-Assurance", "People  strong in the Self-assurance theme feel confident in their ability to manage their own lives. They possess an inner compass that gives them confidence that their decisions are right.", "")):save(),
    (strength:new(id, "Significance", "People  strong in the Significance theme want to be very important in the eyes of others. They are independent and want to be recognized.", "")):save(),
    (strength:new(id, "Strategic", "People strong in the Strategic theme create alternative ways to proceed. Faced with any given scenario, they can quickly spot the relevant patterns and issues.", "")):save(),
    (strength:new(id, "Woo", "People  strong in the Woo theme love the challenge of meeting new people and winning them over. They derive satisfaction from breaking the ice and making a connection with another person.", "")):save().
