-module(export).
-compile(export_all).

owner() ->
    {ok, File} = file:open("owners.erl", [write]),
    lists:map( fun(Owner) ->
		       io:format(File, "(owner:new(~p,~p,~p,~p,~p,~p,~p,~p,~p,~p,~p)):save().~n" ,
				 [Owner:id(),Owner:firstname(),Owner:lastname(),Owner:email(),Owner:ldap_uid(),
				    Owner:permissions(),Owner:groups(),Owner:active(),Owner:userid(),
				    Owner:created_on(),Owner:updated_on()])
	       end,
	       boss_db:find(owner, [])),
    file:close(File).
