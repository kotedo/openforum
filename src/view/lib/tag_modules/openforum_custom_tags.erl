-module(openforum_custom_tags).
-compile(export_all).

% put custom tags in here, e.g.
%
% reverse(Variables, Options) ->
%     lists:reverse(binary_to_list(proplists:get_value(string, Variables))).
%
% {% reverse string="hello" %} => "olleh"
%
% Variables are the passed-in vars in your template
follow(Variables, _Options) ->
    SubjectId = proplists:get_value(subject, Variables),
    OwnerId = proplists:get_value(owner, Variables),
    Success = proplists:get_value(success, Variables),
    Failed = proplists:get_value(failed, Variables),
    %% error_logger:info_msg("SubjectId: ~p~n"
    %% 			  "OwnerId  : ~p~n", [SubjectId, OwnerId]),

    case boss_db:find_first(subscription, [{object, SubjectId},{owner_id, OwnerId}]) of
 	undefined ->
 	    case Failed of 
		undefined ->
		    [];
		_ ->
		    io_lib:format("~s", [binary_to_list(Failed)])
	    end;
        Subscription ->
	    case Success of
		undefined ->
		    [];
		_ ->
		    io_lib:format("~s", [binary_to_list(Success)])
	    end
    end.

glossary(Variables, _Options) ->
    Name = proplists:get_value(name, Variables),
    [Glossary] = boss_db:find(glossary, [{term, Name}]),
    Glossary:description().

vote(Variables, _Options) ->
    SubjectId = proplists:get_value(subject, Variables),
    OwnerId = proplists:get_value(owner, Variables),
    Success = proplists:get_value(success, Variables),
    Failed = proplists:get_value(failed, Variables),
    %% error_logger:info_msg("SubjectId: ~p~n"                                                                                                                                                                                              
    %%                    "OwnerId  : ~p~n", [SubjectId, OwnerId]),                                                                                                                                                                         
    
    case boss_db:find_first(vote, [{object, SubjectId},{owner_id, OwnerId}]) of
        undefined ->
            case Failed of
                undefined ->
                    [];
		_ ->
		    io_lib:format("~s", [binary_to_list(Failed)])
            end;
        Subscription ->
            case Success of
                undefined ->
                    [];
                _ ->
                    io_lib:format("~s", [binary_to_list(Success)])
            end
    end.

