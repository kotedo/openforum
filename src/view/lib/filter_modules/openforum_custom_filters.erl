-module(openforum_custom_filters).
-compile(export_all).
-include("../../../../include/permissions.hrl").

-define(DEFAULT_USR_IMAGE, "/static/userimages/defaultuser.png").

% put custom filters in here, e.g.
%
% my_reverse(Value) ->
%     lists:reverse(binary_to_list(Value)).
%
% "foo"|my_reverse   => "oof"
picture(UserProfile) ->
    %% error_logger:info_msg("~n~nAvatar: ~p~nUserimage: ~p~n~n~n", [UserProfile:avatar(), UserProfile:userimage()]),
    case UserProfile:avatar() of
        [] ->
            %% We have no avatar, so we return the userimage
            usrimage(UserProfile:userimage());
        Avatar ->
            %% We have an avatar now lets return the avatar
            %% error_logger:info_msg("Avatar: ~p~n", [Avatar]),           
            usrimage(Avatar)
    end.

usrimage(Img) ->
    Image = binary_to_list(Img),
    %% error_logger:info_msg("Meat and Potatoes: ~p~n", [Image]),
    case filelib:is_file(["priv/", Image]) of
        true ->
            Image;
        false ->
            ?DEFAULT_USR_IMAGE
    end.

%%last_thread(Forum) ->
%%    boss_db:find_last(forum, [{forumForum
amount_threads(Forum) ->
    %% length(Forum:threads()).
    boss_db:count(thread, [{forum_id, Forum:id()}]).

%% Permissions: read,create,update,edit,delete
can_moderate(Owner) ->
    lists:member(moderate, Owner:permissions()).
can_edit(Owner) ->
    lists:member(edit, Owner:permissions()).
can_delete(Owner) ->
    lists:member(delete, Owner:permissions()).
can_update(Owner) ->
    lists:member(update, Owner:permissions()).
can_create(Owner) ->
    lists:member(create, Owner:permissions()).

user_level([]) ->
    "";
user_level(Permissions) ->
    case Permissions of
	?USER ->
	    "User";
	?EDITOR ->
	    "Editor";
	?CURATOR ->
	    "Curator";
	?ADMIN ->
	    "Admin";
	Else ->
	    "Abuser"
    end.



%% Turning the Erlang:now() timestamp into Human Readable form
%% See http://erldocs.com/R15B/stdlib/io.html?i=12&search=io:#format/1
%% for help on formatting options.
localtime([]) ->
    "";
localtime(undefined) ->
    "";
localtime(TS) ->
    {{Year, Month, Day},{H,M,S}} = calendar:now_to_local_time(TS),
    lists:flatten(io_lib:format("~2.10.0B/~2.10.0B/~4.10.0B ~2.10.0B:~2.10.0B:~2.10.0B", [Month, Day, Year, H, M, S])).


last_comment([]) ->
	 "";
last_comment(Comments) ->
	(lists:last(Comments)):body().

last_author([]) ->
	"";
last_author(Comments) ->
	Last = lists:last(Comments),
	Owner = boss_db:find(Last:owner_id()),
	Owner:fullname().

last_thread([]) ->
     "";
last_thread(Thread) ->
     lists:last(Thread).

%% last_thread([]) -> [];
%% last_thread(Forum) ->
%%     Res = boss_db:find_last(thread, [{forum_id, Forum:id()}]),
%%     error_logger:info_msg("Forum: ~p~nForumId: ~p~nRes: '~p'~n", [Forum, Forum:id(), Res]),
%%     Res.

show_name([]) ->
    "";
show_name(Id) ->
    case boss_db:find(Id) of
	[] ->
	    "No longer exists";
	undefined ->
	    "No longer exists";
	Data ->
	    Data:name()
    end.

find_id([]) ->
    "";
find_id(Id) ->
    boss_db:find(Id).

if_solved([]) ->
    "";
if_solved(Id) ->
    boss_db:find(topic, [{thread_id, Id}, {solved, true}]).

parse_tags(Id) ->
    binary:split(Id, <<",">>, [global]).
