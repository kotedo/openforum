-module(openforum_help_controller, [Req, SessionID]).
-compile(export_all).

%% -------------------------------------------------------------
%% Created: 03/14/2013 10:25:39 CDT
%% Author : Kai Janson (kai.janson@rackspace.com)
%% Purpose: Show all the help articles available and manage them
%% -------------------------------------------------------------
index('GET', []) ->
    %% Index
    Help = boss_db:find(help, []),
    {ok, [{help, Help}]}.

fetch('GET', ["help-" ++ _ = Id]) ->
    {json, [{data, boss_db:find(Id)}]}.

show('GET', ["help-" ++ _ = Id]) ->
    %% Show
    Help = boss_db:find(Id),
    {ok, [{help, Help}]}.

create('GET', []) ->
    %% Create; renders create page
    {ok, []};
create('POST', []) ->
    %% Create: saves the create page
    Topic = Req:post_param("topic"),
    Category = Req:post_param("category"),
    HelpText = Req:post_param("help_text"),
    %% Add fields here
    NewHelp = help:new(id, Topic, Category, HelpText),
    case NewHelp:save() of
	{ok, _SavedHelp} ->
	    boss_flash:add(SessionID, success, "", "Help successfully created."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Help failed to be created."),
	    {redirect, [{action, "create"}, {errors, Errors}]}
    end.

edit('GET', ["help-" ++ _ = Id]) ->
    %% Edit: renders edit page
    Help = boss_db:find(Id),
    {ok, [{help, Help}]};
edit('POST', ["help-" ++ _ = Id]) ->
    %% Saves the edited record
    OldHelp = boss_db:find(Id),
    Topic = Req:post_param("topic"),
    Category = Req:post_param("category"),
    HelpText = Req:post_param("help_text"),    

    NewHelp = OldHelp:set([
			   {topic, Topic},
			   {category, Category},
			   {help_text, HelpText}
			  ]),
    
    case NewHelp:save() of
	{ok, _Updatedhelp} ->
	    boss_flash:add(SessionID, success, "", "Help successfully updated."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Help failed to update."),
	    {redirect, [{action, "edit"}, {errors, Errors}]}
    end.

delete('DELETE', ["help-" ++ _ = Id]) ->
    %% Deletes a record
    boss_db:delete(Id),
    {redirect, [{action, "index"}]}.
