-module(openforum_faq_controller, [Req, SessionID]).
-compile(export_all).

before_(_) ->
    authentication:logged_in(SessionID, Req).

index('GET', [], _Authentication) ->
    %% Index
    Faqs = boss_db:find(faq, [{active, "on"}], [{order_by, question},ascending]),
    {ok, [{faqs, Faqs}]}.

show(GET, ["faq-" ++ _ = Id], _Authentication) ->
    %% Show
    Faq = boss_db:find(Id),
    {ok, [{faq, Faq}]}.

create('GET', [], _Authentication) ->
    %% Create; renders create page
    {ok, []};
create('POST', [], _Authentication) ->
    %% Create: saves the create page
    Question = Req:post_param("question"),
    Answer = Req:post_param("answer"),
    Active = Req:post_param("active"),
    Now = erlang:now(),
    CreatedOn = Now,
    UpdatedOn = Now,
    NewFaq = faq:new(id, Question, Answer, Active, CreatedOn, UpdatedOn),
    
    case NewFaq:save() of
        {ok, SavedFaq} ->
            boss_flash:add(SessionID, success, "", "Faq successfully created."),
            {redirect, [{action, "index"}]};
        {error, Errors} ->
            boss_flash:add(SessionID, error, "", "Faq failed to be created."),
            {redirect, [{action, "create"}, {errors, Errors}]}
    end.

edit('GET', ["faq-" ++ _ = Id], _Authentication) ->
    %% Edit: renders edit page
    Faq = boss_db:find(Id),
    {ok, [{faq, Faq}]};
edit('POST', ["faq-" ++ _ = Id], _Authentication) ->
    %% Saves the edited record
    OldFaq = boss_db:find(Id),

    Question = Req:post_param("question"),
    Answer = Req:post_param("answer"),
    Active = Req:post_param("active"),
    Now = erlang:now(),
    CreatedOn = Now,

    NewFaq = OldFaq:set([{question, Question},
                         {answer, Answer},
                         {active, Active},
                         {updated_on, erlang:now()}]),
    
    case NewFaq:save() of
        {ok, UpdatedFaq} ->
            boss_flash:add(SessionID, success, "", "Faq successfully updated."),
            {redirect, [{action, "index"}]};
        {error, Errors} ->
            boss_flash:add(SessionID, error, "", "Faq failed to update."),
            {redirect, [{action, "edit"}, {errors, Errors}]}
    end.

delete('GET', ["faq-" ++ _ = Id], _Authentication) ->
    %% Deletes a record
    boss_db:delete(Id),
    {redirect, [{action, "index"}]}.
