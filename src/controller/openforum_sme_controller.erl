-module(openforum_sme_controller, [Req, SessionID]).
-compile(export_all).

index('GET', []) ->
    %% Index
    Smes = boss_db:find(sme, []),
    {ok, [{smes, Smes}]}.

show('GET', ["sme-" ++ _ = Id]) ->
    %% Show
    Sme = boss_db:find(Id),
    {ok, [{sme, Sme}]}.

create('GET', []) ->
    %% Create; renders create page
    {ok, []};
create('POST', []) ->
    %% Create: saves the create page
    Fullname = Req:post_param("fullname"),
    OnTeam = Req:post_param("on_team"),
    IsIndividual = Req:post_param("is_individual"),
    Certifications = Req:post_param("certifications"),
    Description = Req:post_param("description"),
    Expertise = Req:post_param("expertise"),
    Years = Req:post_param("years"),
    Highlights = Req:post_param("highlights"),
    Memo = Req:post_param("memo"),
    Tags = Req:post_param("tags"),
    
    %% Add fields here
    NewSme = sme:new(id, Fullname, OnTeam, IsIndividual, Certifications, Description, Expertise, Years, Highlights, Memo, Tags, erlang:now(), []),
    
    case NewSme:save() of
	{ok, _SavedSme} ->
	    boss_flash:add(SessionID, success, "", "Sme successfully created."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Sme failed to be created."),
	    {redirect, [{action, "create"}, {errors, Errors}]}
    end.

edit('GET', ["sme-" ++ _ = Id]) ->
    %% Edit: renders edit page
    Sme = boss_db:find(Id),
    {ok, [{sme, Sme}]};
edit('POST', ["sme-" ++ _ = Id]) ->
    %% Saves the edited record
    OldSme = boss_db:find(Id),
    Fullname = Req:post_param("fullname"),
    OnTeam = Req:post_param("on_team"),
    IsIndividual = Req:post_param("is_individual"),
    Certifications = Req:post_param("certifications"),
    Description = Req:post_param("description"),
    Expertise = Req:post_param("expertise"),
    Years = Req:post_param("years"),
    Highlights = Req:post_param("highlights"),
    Memo = Req:post_param("memo"),
    Tags = Req:post_param("tags"),

    NewSme = OldSme:set([
			 {fullname, Fullname},
			 {on_team, OnTeam},
			 {is_individual, IsIndividual},
			 {certifications, Certifications},
			 {description, Description},
			 {expertise, Expertise},
			 {years, Years},
			 {highlights, Highlights},
			 {memo, Memo},
			 {tags, Tags}
			]),
    
    case NewSme:save() of
	{ok, _Updatedsme} ->
	    boss_flash:add(SessionID, success, "", "Sme successfully updated."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Sme failed to update."),
	    {redirect, [{action, "edit"}, {errors, Errors}]}
    end.

delete('GET', ["sme-" ++ _ = Id]) ->
    %% Deletes a record
    boss_db:delete(Id),
    {redirect, [{action, "index"}]}.
