-module(openforum_forum_controller, [Req, SessionID]).
-compile(export_all).

before_(Method) ->
    authentication:logged_in(SessionID, Req).

index('GET', [], Authorization) ->
    %% Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID, ldap_uid)}]),
    Owner = Authorization,
    Profile = Owner:profile(),
    Channels = boss_db:find(channel, [], [{include, [forums]}]),
    S1D = boss_db:find_first(strength, [{name, Profile:strength1()}]),
    S2D = boss_db:find_first(strength, [{name, Profile:strength2()}]),
    S3D = boss_db:find_first(strength, [{name, Profile:strength3()}]),
    S4D = boss_db:find_first(strength, [{name, Profile:strength4()}]),
    S5D = boss_db:find_first(strength, [{name, Profile:strength5()}]),
    %% Bookmarks = boss_db:find(bookmark, [{owner_id, Owner:id()}]),
    Bookmarks = Owner:bookmarks(),
    %% Posts = boss_db:find(topic, [], [{limit, 50}]),
    %% Threads = boss_db:find(thread, []),
    {ok, [{channels, Channels}, {owner, Owner}, {s1d, S1D},
                                                {s2d, S2D},
                                                {s3d, S3D},
                                                {s4d, S4D},
                                                {s5d, S5D},
%%                                                {profile, Profile}, {bookmarks, Bookmarks}, {posts, Posts}, {threads, Threads}], [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]}.
                                                {profile, Profile} ], [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]}.

show_forums('GET', [Id], Authentication) ->
    %% Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID, ldap_uid)}]),
    Owner = Authentication,
    Channel = boss_db:find(Id),
    Forums = Channel:forums(),
    Channels = boss_db:find(channel, []),
    Followed = boss_db:find_first(subscription, [{object, Id}, {owner_id, Owner:id()}]),
    Bookmarked = boss_db:find_first(bookmark, [{object, Id}, {owner_id, Owner:id()}]),
    Threads = boss_db:find(thread, []),
    Bookmarks = boss_db:find(bookmark, [{owner_id, Owner:id()}]),
    {ok, [{owner, Owner}, {forums, Forums}, {bookmarks, Bookmarks}, {channels, Channels}, {channel, Channel}, {followed, Followed}, {threads, Threads}, {bookmarked, Bookmarked}], [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]}.

show_threads('GET', [Id], Authentication) ->
    %% Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID, ldap_uid)}]),
    Owner = Authentication,
    Thread = boss_db:find(Id),
    Threads = (boss_db:find(Id)):threads(),
    Channels = boss_db:find(channel, []),
    Groups = boss_db:find(o_group, []),
    Bookmarks = boss_db:find(bookmark, [{owner_id, Owner:id()}]),
    Followed = boss_db:find_first(subscription, [{object, Id}, {owner_id, Owner:id()}]),
    Bookmarked = boss_db:find_first(bookmark, [{object, Id}, {owner_id, Owner:id()}]),
    {ok, [{owner, Owner}, {threads, Threads}, {thread, Thread}, {bookmarks, Bookmarks}, {groups, Groups}, {channels, Channels}, {followed, Followed}, {bookmarked, Bookmarked}], [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]}.

show_topics('GET', [Id], Authentication) ->
    %% Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID, ldap_uid)}]),
    Owner = Authentication,
    Thread = boss_db:find(Id),
    Topics = lists:sort(fun(X,Y) -> Y:created_on() > X:created_on() end, (boss_db:find(Id)):topics()),
    VoteTopics = lists:sort(fun(X, Y) -> Y:votes() < X:votes() end, (boss_db:find(Id)):topics()), 
    Channels = boss_db:find(channel, []),
    Groups = boss_db:find(o_group, []),
    Vote = boss_db:find(vote, [{owner_id, Owner:id()}, {object, Id}]),
    TopicVotes = boss_db:find(vote, [{owner_id, Owner:id()}]),
    Bookmarks = boss_db:find(bookmark, [{owner_id, Owner:id()}]),
    Solved = boss_db:find_first(topic, [{thread_id, Id}, {solved, true}]),
    Followed = boss_db:find_first(subscription, [{object, Id}, {owner_id, Owner:id()}]),
    {ok, [{owner, Owner}, {topics, Topics}, {thread, Thread}, {bookmarks, Bookmarks}, {solved, Solved}, {followed, Followed}, {votetopics, VoteTopics}, {topicvotes, TopicVotes}, {channels, Channels}, {groups, Groups}, {vote, Vote}], [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]}.

list_threads('GET', [Id], Authentication) ->
    Threads = (boss_db:find(Id)):threads(),
    {ok, [{threads, Threads}], [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]}.

show_user('GET', [Id], Authentication) ->
    Profile = boss_db:find(Id),
    %% Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID, ldap_uid)}]),
    Owner = Authentication,
    Forums = boss_db:find(forum, []),
    Channels = boss_db:find(channel, []),
    {ok, [{owner, Owner}, {forums, Forums}, {channels, Channels}, {profile, Profile}], [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]}.
