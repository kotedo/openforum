-module(openforum_dashboard_controller, [Req, SessionID]).
-compile(export_all).

before_(_) ->    
    authentication:logged_in(SessionID, Req).

index('GET', [], Authentication) ->
    Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID, ldap_uid)}]),
%%    Owners = boss_db:find(owner, []),
    Channels = boss_db:find(channel, []),
    Forums = boss_db:find(forum, []),
    Glossary = boss_db:find(glossary, []),
    Help = boss_db:find(help, []),
    Bookmarks = boss_db:find(bookmark, [{owner_id, Owner:id()}]),
    {ok, [{owner, Authentication}, {channels, Channels}, {forums, Forums}, {glossaries, Glossary}, {help, Help}, {bookmarks, Bookmarks}]}.

%% all_users('GET', [Id], Authentication) ->
%%     OneUsers = lists:map( fun(X) -> [X:id(), X:fullname(), X:permissions()] end , boss_db:find(owner, [{fullname, 'contains', Id}])),
%%     Users = {json, [{owner, OneUsers}]},
%%     {ok, [{users, Users}]}.
    
    
