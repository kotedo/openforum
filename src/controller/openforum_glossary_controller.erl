-module(openforum_glossary_controller, [Req, SessionID]).
-compile(export_all).

index('GET', []) ->
    %% Index
    Glossary = boss_db:find(glossary, []),
    {ok, [{glossary_items, Glossary}]}.

show('GET', ["glossary-" ++ _ = Id]) ->
    %% Show
    Glossary = boss_db:find(Id),
    {ok, [{glossary, Glossary}]}.

create('GET', []) ->
    %% Create; renders create page
    {ok, []};
create('POST', []) ->
    %% Create: saves the create page
    Term = Req:post_param("term"),
    Description = Req:post_param("description"),
    %% Add fields here
    NewGlossary = glossary:new(id, Term, Description),
    
    case NewGlossary:save() of
	{ok, _SavedGlossary} ->
	    boss_flash:add(SessionID, success, "", "Glossary successfully created."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Glossary failed to be created."),
	    {redirect, [{action, "create"}, {errors, Errors}]}
    end.

edit('GET', ["glossary-" ++ _ = Id]) ->
    %% Edit: renders edit page
    Glossary = boss_db:find(Id),
    {ok, [{glossary, Glossary}]};
edit('POST', ["glossary-" ++ _ = Id]) ->
    %% Saves the edited record
    Term = Req:post_param("term"),
    Description = Req:post_param("description"),

    OldGlossary = boss_db:find(Id),
    NewGlossary = OldGlossary:set([
				   {term, Term},
				   {description, Description}
				  ]),
    
    case NewGlossary:save() of
	{ok, _Updatedglossary} ->
	    boss_flash:add(SessionID, success, "", "Glossary successfully updated."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Glossary failed to update."),
	    {redirect, [{action, "edit"}, {errors, Errors}]}
    end.

delete('DELETE', ["glossary-" ++ _ = Id]) ->
    %% Deletes a record
    boss_db:delete(Id),
    {redirect, [{action, "index"}]}.
