-module(openforum_image_controller, [Req, SessionID]).
-compile(export_all).

%% TODO: before filter

upload_image('POST', []) ->
    {redirect, [{action, "index"}]}.


index('GET', []) ->
    %% Index
    Images = boss_db:find(image, []),
    {ok, [{images, Images}]}.

show('GET', ["image-" ++ _ = Id]) ->
    %% Show
    Image = boss_db:find(Id),
    {ok, [{image, Image}]}.

create('GET', []) ->
    %% Create; renders create page
    {ok, []};
create('POST', []) ->
    %% Create: saves the create page
    Name = Req:post_param("name"),
    Path = Req:post_param("path"),
    Image = Req:post_param("image"),
    Description = Req:post_param("description"),

    %% Add fields here
    NewImage = image:new(id, Image),
    
    case NewImage:save() of
	{ok, _SavedImage} ->
	    boss_flash:add(SessionID, success, "", "Image successfully created."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Image failed to be created."),
	    {redirect, [{action, "create"}, {errors, Errors}]}
    end.

edit('GET', ["image-" ++ _ = Id]) ->
    %% Edit: renders edit page
    Image = boss_db:find(Id),
    {ok, [{image, Image}]};
edit('POST', ["image-" ++ _ = Id]) ->
    %% Saves the edited record
    OldImage = boss_db:find(Id),
    Name = Req:post_param("name"),
    Path = Req:post_param("path"),
    Description = Req:post_param("description"),

    NewImage = OldImage:set([
			     {name,Name},
			     {path, Path},
			     {description, Description},
			     {modified_on, erlang:now()}
			    ]),
    
    case NewImage:save() of
	{ok, _Updatedimage} ->
	    boss_flash:add(SessionID, success, "", "Image successfully updated."),
	    {redirect, [{action, "index"}]};
	{error, Errors} ->
	    boss_flash:add(SessionID, error, "", "Image failed to update."),
	    {redirect, [{action, "edit"}, {errors, Errors}]}
    end.

delete('GET', ["image-" ++ _ = Id]) ->
    %% Deletes a record
    boss_db:delete(Id),
    {redirect, [{action, "index"}]}.
