-module(openforum_profile_controller, [Req, SessionID]).
-compile(export_all).

before_(Method) ->
    authentication:logged_in(SessionID, Req).

show_profile('GET', [], Authentication) ->
    Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID, ldap_uid)}]),
    Profile = Owner:profile(),
    Channels = boss_db:find(channel, []),
    Forums = boss_db:find(forum, []),
    Threads = boss_db:find(thread, []),
    S1D = boss_db:find_first(strength, [{name, Profile:strength1()}]),
    S2D = boss_db:find_first(strength, [{name, Profile:strength2()}]),
    S3D = boss_db:find_first(strength, [{name, Profile:strength3()}]),
    S4D = boss_db:find_first(strength, [{name, Profile:strength4()}]),
    S5D = boss_db:find_first(strength, [{name, Profile:strength5()}]),
    Posts = boss_db:find(topic, [{owner_id, Owner:id()}]),
    Bookmarks = boss_db:find(bookmark, [{owner_id, Owner:id()}]),
    {ok, [{channels, Channels}, {forums, Forums}, {threads, Threads}, {owner, Owner}, {s1d, S1D},
	                                                        {s2d, S2D},
	                                                        {s3d, S3D},
                                                                {s4d, S4D},
                                                                {s5d, S5D},
                                                                {posts, Posts}, {bookmarks, Bookmarks}, {profile, Profile}], [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]}.

edit_profile('GET', [], Authentication) ->
    %% Owner = boss_db:find(owner, [{ldap_uid, boss_session:get_session_data(SessionID,ldap_uid)}]),
    {ok, [{owner, Authentication}]};
edit_profile('POST', [], Authentication) ->
    AboutMe = Req:post_param("about_me"),
    Signature = Req:post_param("signature"),
    %% Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID,ldap_uid)}]),
    OldProfile = Authentication:profile(),
    NewProfile = OldProfile:set([{about_me, AboutMe}, {signature, Signature}]),
    NewProfile:save(),
    {redirect, [{controller, "profile"}, {action, "show_profile"}]}.
