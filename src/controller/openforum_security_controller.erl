-module(openforum_security_controller, [Req, SessionID]).
-compile(export_all).

login('GET', []) ->
    %% Renders the login screen.
    ok;
login('POST', []) ->
    %% Here is the interesting stuff
    SSO = Req:post_param("sso_username"),
    Password = Req:post_param("sso_password"),
    case ldap:login(SessionID, SSO, Password) of
	{ok, MyLdapInfo} ->
	    %% error_logger:info_msg("MYLDAPCRAP: ~p~n", [MyLdapInfo]),
	    {redirect, [{controller, "forum"}, {action, "index"}]};
	{error, Reason} ->
	    error_logger:error_msg("(SECURITY): An error occurred: ~p~n", [Reason]),
	    ErrorMsg = case Reason of
			   anonymous_auth ->
			       "Please enter a username and password";
			   invalidCredentials ->
			       "Wrong username / password";
			   OtherReason ->
			       lists:flatten(io_lib:format("An unknown error ocurred: ~p", [OtherReason]))
			   end,
	    boss_flash:add(SessionID, error, "We got an error: ", ErrorMsg),
	    {redirect, [{action, "login"}, {errors, ErrorMsg}]};
	AnythingElse ->
	    error_logger:info_msg("(SECURITY): Something else happened: ~p~n", [AnythingElse]),
	    boss_flash:add(SessionID, error, "We got an error")
    end.

logout('GET', []) ->
    ldap:logout(SessionID),
    {redirect, [{controller, "security"}, {action, "login"}]}.
