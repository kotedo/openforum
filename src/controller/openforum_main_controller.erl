-module(openforum_main_controller, [Req, SessionID]).
-compile(export_all).
-include("../../include/permissions.hrl").

%%cache_("index", []) ->
%%    {page, [{seconds, 3600}]};
cache_("profile", [ProfileId]) ->
    {vars, [{seconds, 300}, {watch, ProfileId ++ ".*"}]};
cache_("vote", [Id]) ->
    none;
cache_("create_thread", [ThreadId]) ->
    none;
cache_("create_topic", [TopicId]) ->
    none;
cache_(_, _) ->
    none.

before_(_) ->
    authentication:logged_in(SessionID, Req).

have_forum('GET', [Id], _Authentication) ->
    SearchTerm = mochiweb_util:unquote(Id),
    case boss_db:find(forum, [{name, "*" ++ SearchTerm}]) of
        [] ->
            %% No result, we are good to give the green light to create the forum
            error_logger:info_msg("empty NOT found: ~p~n", [Id]),
            {json, [{status, 200}]};
        _Data ->
            %% Any response means we have that forum name already, cancel creation
            error_logger:info_msg("FOUND: ~p~n", [Id]),
            {json, [{status, 409}, {errorText, "Already exists!"}]}
    end.

search_all_tags('POST', [], _Authentication) ->
    Tags = lists:map(fun(X) -> X:tags() end, boss_db:find(thread, [])),
    Tag = proplists:delete(undefined, Tags),
    Long_Tags = lists:map(fun(X) ->
                                  case is_binary(X) of
                                      true ->
                                          %% error_logger:info_msg("X is binary ~p~n", [X]),
                                          case string:str(binary_to_list(X), ",") of
                                              0 ->
                                                  %% error_logger:info_msg("X (~p) is binary split on ' ' to '~p'~n", [X, A0]),
                                                  %% A0;
                                                  binary:split(X, [<<" ">>], [global]);
                                              _N1 ->
                                                  %%error_logger:info_msg("X (~p) is binary split on ',' to '~p'~n", [X, A1]),
                                                  %% A1
                                                  binary:split(X, [<<",">>], [global])
                                          end;
                                      _ ->
                                          %% error_logger:info_msg("X is NOT binary ~p~n", [X]),
                                          case string:str(X, ",") of
                                              0 ->
                                                  %% error_logger:info_msg("X (~p) is string tokens on ' ' to '~p'~n", [X, B0]),
                                                  %% B0;
                                                  string:tokens(X, " ");
                                              _N2 ->
                                                  %% error_logger:info_msg("X (~p) is string tokens on ',' to '~p'~n", [X, B1]),
                                                  %% B1
                                                  string:tokens(X, ",")
                                          end
                                  end
                          end, Tag),
    Long_Tag = lists:merge(Long_Tags),
    Long_Tag_2 = sets:from_list(Long_Tag), %% Removes Dupes
    Long_Tag_3 = sets:to_list(Long_Tag_2), %% Removes Dupes
    AllTags = lists:map(fun(X) ->
                                [{tag, X}]
                        end, Long_Tag_3),
    {json, [{tags, AllTags}]}.

notification('GET', [Id], _Authentication) ->
    {ok, Timestamp, Messages} = boss_mq:pull(Id, now),
    {json, [{messages , Messages}]}.

remove_forum('GET', ["forum-" ++ _ = Id], _Authentication) ->
    boss_db:delete(Id),
    {redirect, [{controller, "main"}, {action, filename:basename(Req:header(referer))}]}.

new_delete('DELETE', [Id], Authentication) ->
    boss_db:delete(Id),
    {json, [{status, 200}]}.

edit_avatar('GET', [], _Authentication) ->
    %% error_logger:info_msg("Auth: ~p~n", [Authentication]),
    {redirect, [{controller, "profile"}, {action, "show_profile"}]};
edit_avatar('POST', [], Authentication) ->
    error_logger:info_msg("Auth: ~p~n", [Authentication]),
    error_logger:info_msg("PostFiles: ~p~n", [Req:post_files()]),
    %% [{uploaded_file, _Filename, _TempRewardPicture, _PictureSize}] = Req:post_files(),
    AvatarPath = file_lib:avatar(Req:post_files(), boss_session:get_session_data(SessionID, username)),
    Profile = Authentication:profile(),
    (Profile:set([{avatar, AvatarPath}])):save(),
    {redirect, [{controller, "profile"}, {action, "show_profile"}]}.

edit_forum('GET', [], _Authentication) ->
    Owners = boss_db:find(owner, []),
    Channels = boss_db:find(channel, []),
    Forums = boss_db:find(forum, []),
    {redirect, [{controller, "dashboard"}, {action, "index"}]};
edit_forum('GET', [Id], Authentication) ->
    %% Owner = boss_db:find_first(owner, [{ldap_uid, boss_session:get_session_data(SessionID, ldap_uid)}]),
    Owner = Authentication,
    Owners = boss_db:find(owner, []),
    Channels = boss_db:find(channel, []),
    Forums = boss_db:find(forum, []),
    Forum = boss_db:find(Id),
    Bookmarks = boss_db:find(bookmark, [{owner_id, Owner:id()}]),
    {ok, [{owners, Owners}, {channels, Channels}, {forums, Forums}, {forum, Forum}, {owner, Owner}, {bookmarks, Bookmarks}]};
edit_forum('POST', [Id], _Authentication) ->
    OldForum = boss_db:find(Id),
    OwnerId = Req:post_param("owner_id"),
    ChannelId = Req:post_param("channel_id"),
    ForumName = Req:post_param("forum_name"),
    Comments = Req:post_param("comments"),
    NewForum = OldForum:set([
                             {owner_id, OwnerId},
                             {channel_id, ChannelId},
                             {name, ForumName},
                             {comments, Comments},
                             {updated_on, erlang:now()}
                            ]),
    case NewForum:save() of
        {ok, _UpdatedForum} ->
            {redirect, [{controller, "main"}, {action, "edit_forum"}]};
        {error, Reason} ->
            Reason
    end.

create_topic('POST', [], _Authentication) ->
    %% Old: [Id, ThreadId, OwnerId, Name, Tags, Body, CreatedOn, UpdatedOn]
    %% New: [Id, ThreadId, OwnerId, Name, Tags, Body, Solved, SolvedBy, SolvedOn, CreatedOn, UpdatedOn]).
    ThreadId = Req:post_param("thread_id"),
    OwnerId = Req:post_param("owner_id"),
    TopicName = Req:post_param("topic_name"),
    Tags = Req:post_param("tags"),
    Body = Req:post_param("body"),
    %% These will be set by an AJAX call and are used for indentifying that a discussion has come to a
    %% sanctioned solution.
    Solved = false,
    SolvedBy = [],
    SolvedOn = [],
    Votes = 0,
    
    case (topic:new(id, ThreadId, OwnerId, TopicName, Tags, Body, Solved, SolvedBy, SolvedOn, Votes, erlang:now(), [])):save() of
        {ok, _SavedTopic} ->
            %% do_notification(ThreadId),
            error_logger:info_msg("Topic Created: ~p~n",[ThreadId]),
            {_,_,CaA,_,_} = mochiweb_util:urlsplit(Req:header(referer)),
            case string:tokens(CaA, "/") of
                [Controller, Action, Thread] ->
                    {redirect, [{controller, Controller},{action, Action}, {id, Thread}]};
                [Controller, Action] ->
                    {redirect, [{controller, Controller},{action, Action}]};
                AnythingElse ->
                    error_logger:info_msg("Pattern did not match anything: ~p~n", [AnythingElse]),
                    AnythingElse
            end;
        {error, Reason} ->
            error_logger:info_msg("Reason: ~p~n", [Reason]),
            Reason
    end.

edit_topic('GET', [TopicId], Authentication) ->
    Topic = boss_db:find(TopicId),
    {ok, [{topic, Topic}]};
edit_topic('POST', [TopicId], Authentication) ->
    OldTopic = boss_db:find(TopicId),
    Body = Req:post_param("body"),
    %% New: [Id, ThreadId, OwnerId, Name, Tags, Body, Solved, SolvedBy, SolvedOn, CreatedOn, UpdatedOn]).
    NewTopic = OldTopic:set([{body, Body}, {updated_on, erlang:now()}]),
    
    case NewTopic:save() of
        {ok, _UpdatedTopic} ->
            {_,_,CaA,_,_} = mochiweb_util:urlsplit(Req:header(referer)),
            case string:tokens(CaA, "/") of
                [Controller, Action, Thread] ->
                    {redirect, [{controller, Controller},{action, Action}, {id, Thread}]};
                [Controller, Action] ->
                    {redirect, [{controller, Controller},{action, Action}]};
                AnythingElse ->
                    error_logger:info_msg("Pattern did not match anything: ~p~n", [AnythingElse]),
                    AnythingElse
            end;
        {error, Reason} ->
            error_logger:info_msg("Reason: ~p~n", [Reason]),
            Reason
    end.

fetch_topic_body('GET', [TopicId], _Authentication) ->
    Data = boss_db:find(TopicId),
    {json, [{body, Data:body()}]}.

fetch_user_info('GET', [Id], _Authentication) ->
    error_logger:info_msg("SearchUser: ~p~n", [Id]),
    %% Trying to find the user in the local database, searching case insensitive
    Firstnames = boss_db:find(owner, [{firstname, matches, "*" ++ Id}]),
    Lastnames = boss_db:find(owner, [{lastname, matches, "*" ++ Id}]),
    Users = lists:sort(Firstnames ++ Lastnames),
    %% Users = boss_db:find(owner, [{firstname, matches, Id}]),
    {ok, [{users, Users}, {results, length(Users)}]}.

remove_topic('DELETE', ["topic-" ++ _ = Id], _Authentication) ->
    boss_db:delete(Id),
    {json, [{status, 200}]}.

%% Delete a thread and trash all associated topics (posts) with it.
remove_thread('DELETE', ["thread-" ++ _ = Id], _Authentication) ->
    Thread = boss_db:find(Id),
    [fun(Topic) -> boss_db:delete(Topic:id()) end || Topic <- Thread:topics()],
    boss_db:delete(Id),
    {json, [{status, 200}]}.

create_thread('POST', [], _Authentication) ->
    %% Id, ForumId, OwnerId, Name, Comment, CreatedOn, UpdatedOn
    ForumId = Req:post_param("forum_id"),
    OwnerId = Req:post_param("owner_id"),
    ThreadName = Req:post_param("thread_name"),
    Comments = Req:post_param("comments"),
    Tags = Req:post_param("hidden-tags"),
    error_logger:info_msg("~p~n", [Tags]),
    Open = true,
    Votes = 0,
    Oldid = 0,
    
    case (thread:new(id, ForumId, OwnerId, ThreadName, Comments, Tags, Open, Votes, Oldid, erlang:now(), [])):save() of
        {ok, SavedThread} ->
            %% 04/26/2013 00:22:15: "Redirecting to newly created discussion" added as of PivotalTracker ID #48025009
            {redirect, "/forum/show_topics/" ++ SavedThread:id()};
        {error, Reason} ->
            Reason
    end.

create_forum('GET', [], Authentication) ->
    Owners = boss_db:find(owner, []),
    Channels = boss_db:find(channel, []),
    Forums = boss_db:find(forum, []),
    {ok, [{owners, Owners}, {channels, Channels}, {forums, Forums}, {owner, Authentication}]};
create_forum('POST', [], _Authentication) ->
    %% error_logger:info_msg("Req:protcol(): ~p~n", [Req:protocol()]),
    %% OwnerId   = (boss_db:find_first(owner, [])):id(),
    OwnerId = (boss_db:find_first(owner, [{firstname, "Admin"}])):id(),
    ChannelId = Req:post_param("channel_id"),
    ForumName = Req:post_param("forum_name"),
    Comments  = Req:post_param("comments"),
    Tags = Req:post_param("tags"),
    %%     (forum, [Id, ChannelId, OwnerId, Name, Comments, Tags,        CreatedOn, UpdatedOn])
    %%     (forum, [Id, ChannelId, OwnerId, Name, Comments, Tags, Oldid, CreatedOn, UpdatedOn]).
    Oldid = [],
    
    case (forum:new(id, ChannelId, OwnerId, ForumName, Comments, Tags, Oldid, erlang:now(), [])):save() of
        {ok, _SavedForum} ->
            {redirect, [{controller, "dashboard"},{action,"index"}]};
        {error, Reason} ->
            Reason
    end.

create_channel('GET', [], Authentication) ->
    Channels = boss_db:find(channel, []),
    {ok, [{channels, Channels}, {owner, Authentication}]};
create_channel('POST', [], _Authentication) ->
    ChannelName = Req:post_param("channel_name"),
    Description = Req:post_param("description"),
    Now = erlang:now(),
    Tags = Req:post_param("tags"),
    
    %% [Id, Name, OwnerId, CreatedOn, UpdatedOn])
    %% [Id, Name, OwnerId, Description, Position, Tags, Userid, Oldid, CreatedOn, UpdatedOn]).
    
    OwnerId = (boss_db:find_first(owner, [])):id(),
    Position = 0, %% Meaning, no position ... we shall see
    
    Userid = 0,
    Oldid = 0,
    
    case (channel:new(id, ChannelName, OwnerId, Description, Position, Tags, Userid, Oldid, Now, Now)):save() of
        {ok, _NewChannel} ->
            {redirect, [{controller, "dashboard"},{action, "index"}]};
        {error, Reason} ->
            Reason
    end.

%%
%% ======================== Extra Stuff goes here =====================
%%
perpage('PUT', [PerPage], Authentication) ->
    Profile = Authentication:profile(),
    (Profile:set([{per_page, PerPage}])):save(),
    %% Please make sure we have the "flash" stuff back in the header to show messages
    boss_flash:add(SessionID, success, "", "Per Page updated"),
    {json, [{status, 200}]}.

%% Use VisualJSON to ensure proper structures (Download from AppStore)
follow('PUT', [Id], Authentication) ->
    error_logger:info_msg("Id: ~p~n", [Id]),
    (subscription:new(id, Authentication:id(), Id, erlang:now())):save(),
    {json, [{status, 200}]};
follow('DELETE', [Id], _Authentication) ->
    boss_db:delete(Id),
    {json, [{status, 200}]}.

frequency('PUT', [Id, Frequency], Authentication) ->
    error_logger:info_msg("Id: ~p~n", [Id]),
    Subscription = boss_db:find(Id),
    (Subscription:set([{frequency, Frequency}])):save(),
    %% Please make sure we have the "flash" stuff back in the header to show messages
    boss_flash:add(SessionID, success, "", "Follow updated"),
    {json, [{status, 200}]}.

%% All about bookmarks here
bookmark('GET', [], Authentication) ->
    %% get all bookmarks
    Bookmarks = boss_db:find(bookmark, [{owner_id, Authentication:id()}]),
    {ok, [{bookmarks, Bookmarks}]};
bookmark('PUT', [Id], _Authentication) ->
    %% set a bookmark
    error_logger:info_msg("Id: ~p~n", [Id]),
    (bookmark:new(id, _Authentication:id(), Id, erlang:now())):save(),
    {json, [{status, 200}]};
bookmark('DELETE', [Id], _Authentication) ->
    %% remove a bookmark
    boss_db:delete(Id),
    {json, [{status, 200}]}.

%% This is about removing the avatar from the user profile
%% and reverting back to the LDAP provided userimage
avatar('DELETE', ["owner-" ++ _ = Id], Authentication) ->
    %% Reset it now
    Profile = Authentication:profile(),
    (Profile:set([{avatar, []}])):save(),
    {json, [{status, 200}]}.

%% Topics can be resolved and ready for transfer into OpenKB
solved('PUT', [Id,OwnerId], Authentication) ->
    Topic = boss_db:find(Id),
    (Topic:set([
                {solved, true},
                {solved_by, OwnerId},
                {solved_on, erlang:now()}
               ])):save(),
    {json, [{status, 200}]};

%% Or not.  Then they need to be rejected. Will trigger OpenKB
%% to display a note that it has been rejected as "solved".
solved('DELETE', [Id], Authentication) ->
    Topic = boss_db:find(Id),
    (Topic:set([
                {solved, false},
                {solved_by, []},
                {solved_on, []}
               ])):save(),
    {json, [{status, 200}]}.

%% Setting the locale to the selected one
locale('PUT', [Locale], Authentication) ->
    Profile = Authentication:profile(),
    (Profile:set([{locale, Locale}])):save(),
    boss_session:set_session_data(SessionID, locale, Locale),
    {json, [{status, 200}]};
locale('DELETE', [Locale], Authentication) ->
    Profile = Authentication:profile(),
    (Profile:set([{locale, "en-us"}])):save(),
    boss_session:set_session_data(SessionID, locale, "en-us"),
    {json, [{status, 200}]}.

%% Voting takes place here
vote('PUT', [OwnerId, ItemId], _Authentication) ->
    error_logger:info_msg("OwnerId  : ~p~n"
                          "ItemId   : ~p~n",
                          [OwnerId, ItemId]),
    do_up_vote(OwnerId, ItemId),  %% This is the real deal
    {json, [{status, 200}]};
vote('DELETE', [OwnerId, ItemId], _Authentication) ->
    error_logger:info_msg("OwnerId  : ~p~n"
                          "ItemId   : ~p~n",
                          [OwnerId, ItemId]),
    do_down_vote(OwnerId, ItemId),
    {json, [{status, 200}]}.

%% Heart of the voting
do_up_vote(OwnerId, ItemId) ->
    %% Let's see if the Owner (=User) has already voted on this "object"
    Msg = case boss_db:find_first(vote, [{owner_id, OwnerId}, {object, ItemId}]) of
              undefined ->
                  %% No entry in voting table, that means we are good to cast a vote
                  %% Find the old Topic, so that we can manipulate the obejct itself
                  Topic = boss_db:find(ItemId),
                  (Topic:set([{votes, Topic:votes() + 1}])):save(),
                  %% Topic has been updated, added one to the Votes column
                  %% Now we need to create an entry in the Votes tables for this user
                  %% and object the user voted on.
                  (vote:new(id, OwnerId, Topic:id(), erlang:now())):save(),
                  boss_flash:add(SessionID, success, "", "Your up vote has been recorded."),
                  lists:flatten(io_lib:format("Vote recorded for ~s", [Topic:name()]));
              _AnythingElse ->
                  %% Since the owner has already voted on it, no additional voting allowed.
                  lists:flatten(io_lib:format("You already voted on it. Not voting again.", [])),
                  boss_flash:add(SessionID, success, "", "You have already voted on this item.")
          end,
    {json, [{status, 200}, {message, Msg}]}.

do_down_vote(OwnerId, ItemId) ->
    %% Let's see if the Owner (=User) has already voted on this "object"
    Msg = case boss_db:find_first(vote, [{owner_id, OwnerId}, {object, ItemId}]) of
              undefined ->
                  %% No entry in voting table, that means we are good to cast a vote
                  %% Find the old Topic, so that we can manipulate the obejct itself
                  Topic = boss_db:find(ItemId),
                  (Topic:set([{votes, Topic:votes() - 1}])):save(),
                  
                  %% Topic has been updated, added one to the Votes column
                  %% Now we need to create an entry in the Votes tables for this user
                  %% and object the user voted on.
                  (vote:new(id, OwnerId, Topic:id(), erlang:now())):save(),
                  boss_flash:add(SessionID, success, "", "Your down vote has been recorded."),
                  lists:flatten(io_lib:format("Vote withdrawn for ~s.", [Topic:name()]));
              _AnythingElse ->
                  %% Since the owner has already voted on it, no additional voting allowed.
                  lists:flatten(io_lib:format("No change made.", [])),
                  boss_flash:add(SessionID, success, "", "You have already voted on this item.")
          end,
    {json, [{status, 200}, {message, Msg}]}.

%% Dealing with threads; closing and reopening them
close_thread('PUT', [Id], Authentication) ->
    Thread = boss_db:find(Id),
    (Thread:set([{open, false}])):save(),
    {json, [{status, 200}]}.

reopen_thread('PUT', [Id], Authentication) ->
    Thread = boss_db:find(Id),
    (Thread:set([{open, true}])):save(),
    {json, [{status, 200}]}.

%% Promoting owners to different levels
change_user_level('PUT', [Permission, OwnerId], _Authentication) ->
    Owner = boss_db:find(OwnerId),
    {NewPermissions, GroupId} = case Permission of
                                    "user" ->
                                        {?USER, (boss_db:find_first(o_group, [{name, "user"}])):id()};
                                    "admin" ->
                                        {?ADMIN, (boss_db:find_first(o_group, [{name, "admin"}])):id()}
                                end,
    (Owner:set([{permissions, NewPermissions}, {groups, [GroupId]}])):save(),
    {json, [{status, 200}]}.

permissions('PUT', [Id], Authentication) ->
    Owner = boss_db:find(Id),
    NewPermissions = case Owner:permissions() of
                         ?USER ->
                             ?EDITOR;
                         ?EDITOR ->
                             ?CURATOR;
                         ?CURATOR ->
                             ?ADMIN;
                         ?ADMIN ->
                             ?ADMIN
                     end,
    (Owner:set([{permissions, NewPermissions}])):save(),
    {json, [{status, 200}]};
%% Promoting owners to different levels
permissions('DELETE', [Id], Authentication) ->
    Owner = boss_db:find(Id),
    NewPermissions = case Owner:permissions() of
                         ?ADMIN ->
                             ?CURATOR;
                         ?CURATOR ->
                             ?EDITOR;
                         ?EDITOR ->
                             ?USER;
                         ?USER ->
                             ?USER
                     end,
    (Owner:set([{permissions, NewPermissions}])):save(),
    {json, [{status, 200}]}.
