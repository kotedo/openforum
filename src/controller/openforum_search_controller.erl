-module(openforum_search_controller, [Req, SessionID]).
-compile(export_all).

%% Disabled caseless search right now ...
%% There are issues with newer MongoDB drivers and THIS
%% version of ChicagoBoss ... no time to find the
%% culprit.

before_(_) ->
    authentication:logged_in(SessionID, Req).

all_forums('GET', []) ->
    {ok, [{data, []}]};
all_forums('GET', [SearchTerm]) ->
    error_logger:info_msg("Forums~n"
			  "SearchTerm: ~p~n", [SearchTerm]),
    %% Data = boss_db:find(forum, [{name, matches, SearchTerm, [caseless] }]),
    Data = boss_db:find(forum, [{name, matches, SearchTerm, [caseless] }]),
    {ok, [{data, Data}]}.

all_threads('GET', []) ->
    {ok, [{data, []}]};
all_threads('GET', [SearchTerm]) ->
    error_logger:info_msg("Threads: SearchTerm: ~p~n", [SearchTerm]),
    %% Data = boss_db:find(thread, [{name, matches, SearchTerm, [caseless] }]),
    Data = boss_db:find(thread, [{name, matches, SearchTerm}]),
    {ok, [{data, Data}]}.

all_topics('GET', []) ->
    {ok, [{data, []}]};
all_topics('GET', [SearchTerm]) ->
    error_logger:info_msg("Topics: SearchTerm: ~p~n", [SearchTerm]),
    %% Data = boss_db:find(topic, [{body, matches, SearchTerm, [caseless] }]),
    Data = boss_db:find(topic, [{body, matches, SearchTerm}]),
    {ok, [{data, Data}]}.

all_search('GET', []) ->
    {ok, [{data, []}]};
all_search('GET', [SearchTerm]) ->
    error_logger:info_msg("Topics: SearchTerm: ~p~n", [SearchTerm]),
    DataTopics = boss_db:find(topic, [{body, matches, "*" ++ SearchTerm}]),
    DataThreads = boss_db:find(thread, [{name, matches, "*" ++ SearchTerm}]),
    DataForums = boss_db:find(forum, [{name, matches, "*" ++ SearchTerm}]),
    {ok, [{datatopics, DataTopics}, {datathreads, DataThreads}, {dataforums, DataForums}]}.

tag_search('GET', []) ->
    {ok, [{data, []}]};
tag_search('GET', [SearchTerm]) ->
    %% DataThreads = boss_db:find(thread, [{tags, matches, SearchTerm, [caseless] }]),
    DataThreads = boss_db:find(thread, [{tags, matches, SearchTerm}]),
    {ok, [{datathreads, DataThreads}]}.
