-module(openforum_errors_controller, [Req, SessionID]).
-compile(export_all).

lost_in_404('GET', []) ->
    URI = Req:uri(),
    error_logger:info_msg("ERRRRRRROR (404): ~p~n", [Req]),
    {ok, [{uri, URI}]}.


calamity('GET', []) ->
    ok.

calamity('GET', [], _Authentication) ->
    ok.
