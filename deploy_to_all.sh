#!/bin/bash
APPSERVERS=`cat servers.config`

if [ "X"$1 == "X" ] ; then
    echo "You MUST set a destination on the servers; typically it's \$HOME, so you can use '.' here."
    exit 1
else
    DESTINATION=$1
fi

for i in $APPSERVERS
{
echo "Working on app server $i (in $DESTINATION)"
ssh rack@$i.openforum.rackspace.corp "(cd $DESTINATION && rm -rf openforum && git clone git@repoman.rackspace.com:openforum.git && cd openforum && bash sethostname.sh)";
echo "Done with app server $i"
echo ""
}
