#!/bin/bash

echo "Pulling in the git changes, re-compiling the project and re-loading it w/o affecting users"
git pull && ./rebar compile > /dev/null && ./init.sh reload
