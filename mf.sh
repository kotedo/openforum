#!/bin/bash

## Functions to install, maintain and get status information for OpenForum


##############################################################################
#                                                                            #
# Deploys OpenForum to all servers.  Destination must be set to a location   #
# usually set as the home directory of the 'rack' user.                      #
# Thus "." as the destination directory is the assumed default.              #
# If you want to install the code in a different diretory (i.e. for testing) #
# you could specify "/tmp" as the destination.                               #
#                                                                            #
##############################################################################
function deploy()
{
    APPSERVERS=`cat servers.config`

    if [ "X"$1 == "X" ] ; then
	echo "You MUST set a destination on the servers; typically it's \$HOME, so you can use '.' here."
	exit 1
    else
	DESTINATION=$1
    fi
    
    for i in $APPSERVERS
    {
	echo "Working on app server $i (in $DESTINATION)"
	ssh rack@$i.openforum.rackspace.corp "(cd $DESTINATION && rm -rf openforum && git clone git@repoman.rackspace.com:openforum.git && cd openforum && bash sethostname.sh)";
	echo "Done with app server $i"
	echo ""
    }
}

##############################################################################
#                                                                            #
# Updates OpenForum on all servers.                                          #
# Performs these steps:                                                      #
#   * pulls the latest changes from the git repo                             #
#   * cleans and recompiles the code                                         #
#   * uses Erlang's Hot-Code Reload functionality to reload live code        #
#     which does not affect the users, sessions and settings will remain     #
#                                                                            #
#     SAFE FOR PRODUCTION                                                    #
##############################################################################
function update()
{
    echo "Pulling in the git changes, re-compiling the project and re-loading it w/o affecting users"
    git pull && ./rebar clean compile > /dev/null && ./init.sh reload
}



##############################################################################
# Patches boss.config to reflect Main Node and Worker Node settings          #
#                                                                            #
# Performs these steps:                                                      #
#   * sets the correct hostname for the Erlang VM                            #
#   * finds out if the node is the master node or a worker node              #
#   * compiles the code                                                      #
#                                                                            #
##############################################################################
function patch()
{
    echo "Copying boss.config.prod to boss.config..."
    cp boss.config.prod boss.config

    echo "Setting vm_name to openforum@$HOSTNAME"
    perl -p -i -e "s/HOSTNAME/$HOSTNAME/g" boss.config

    echo "Finding out if we are the MasterNode and patching boss.config accordingly"
    if [ "$HOSTNAME" == "dfw-app001.openforum.rackspace.corp" ] ; then
	echo " -> We ARE the master node so we are removing the {master_node, ''} setup plus the leading comment."
	sed -i -e "/%% .*the boss\?/d" boss.config
	sed -i -e "/master_node/d"  boss.config
    else
	echo " -> We are not the master node, so we are linking to the master node"
    fi

    echo "Compiling the project"
    ./rebar get-deps compile > /dev/null
}



##############################################################################
#                                                                            #
# Updates all servers                                                        #
#                                                                            #
##############################################################################
function update_all()
{
    APPSERVERS=`cat servers.config`

    if [ "X"$1 == "X" ] ; then
	echo "You MUST set a destination on the servers; typically it's \$HOME, so you can use '.' here."
	exit 1
    else
	DESTINATION=$1
    fi

    for i in $APPSERVERS
    {
	echo "Working on updating app server $i (in $DESTINATION)"
	echo " -> Pulling in changes from git, compiling and hot-code reloading the code."
        ## ssh rack@$i.openforum.rackspace.corp "(if [ -d "$DESTINATION" ] ; then (cd $DESTINATION && git pull && ./rebar compile > /dev/null && ./init.sh reload) else echo "No such directory '$DESTINATION'"; fi )"

	ssh rack@$i.openforum.rackspace.corp "(cd openforum && ./gitupdate.sh)" 
	# ssh rack@$i.openforum.rackspace.corp "(cd $DESTINATION && rm -rf openforum && git clone git@repoman.rackspace.com:openforum.git && cd openforum && bash sethostname.sh)";
	echo " Done with app server $i"
	echo ""
    }
}
