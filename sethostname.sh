#!/bin/bash

echo "Copying boss.config.prod to boss.config..."
cp boss.config.prod boss.config

echo "Setting vm_name to openforum@$HOSTNAME"
perl -p -i -e "s/HOSTNAME/$HOSTNAME/g" boss.config

echo "Finding out if we are the MasterNode and patching boss.config accordingly"
if [ "$HOSTNAME" == "dfw-app001.openforum.rackspace.corp" ] ; then
    echo " -> We ARE the master node so we are removing the {master_node, ''} setup plus the leading comment."
    sed -i -e "/%% Who is the boss\?/d" boss.config
    sed -i -e "/{master_node/d"  boss.config
    sed -i -e "s/%% {mail_driver/{mail_driver/" boss.config
    sed -i -e "s/%% {mail_relay_host/{mail_relay_host/" boss.config
    sed -i -e "s/%% {smtp_server_enable/{smtp_server_enable/" boss.config
    sed -i -e "s/%% {smtp_server_address/{smtp_server_address/" boss.config
    sed -i -e "s/%% {smtp_server_domain/{smtp_server_domain/" boss.config
    sed -i -e "s/%% {smtp_server_port/{smtp_server_port/" boss.config
    sed -i -e "s/%% {smtp_server_protocol/{smtp_server_protocol/" boss.config
else
    echo " -> We are not the master node, so we are linking to the master node"
fi

echo "Compiling the project"
./rebar get-deps compile > /dev/null
